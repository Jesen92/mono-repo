json.extract! company, :id, :name, :oib, :address, :username, :password, :license_expires, :created_at, :updated_at
json.url company_url(company, format: :json)
