json.extract! programme_type, :id, :name, :created_at, :updated_at
json.url programme_type_url(programme_type, format: :json)
