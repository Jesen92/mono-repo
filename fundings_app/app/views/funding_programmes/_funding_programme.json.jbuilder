json.extract! funding_programme, :id, :name, :description, :beginning_date, :end_date, :cofinancing_percentage, :purpose_id, :programme_type_id, :city_id, :county_id, :min_amount, :max_amount, :funding_programme_link, :created_at, :updated_at
json.url funding_programme_url(funding_programme, format: :json)
