class NotificationMailer < ApplicationMailer
  default from: 'IZI do potpora <no-respond@izidopotpora.hr>'

  def send_favorites_notifications(user, funding_programme)
    @user = user
    @funding_programme = funding_programme

    mail(to: user.email, subject: "Izmjene na natječaju "+funding_programme.name, template_path: 'mailer', template_name: 'user_notifications') unless user.email.blank?
  end
end
