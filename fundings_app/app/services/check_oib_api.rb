class CheckOibApi
  def initialize(params)
    @oib = params[:oib]
  end

  def check_oib
    #TODO poveži se sa API-em za provjeru OIB-a
    company = Company.find_by(oib: oib)

    return nil unless company.nil?

    #TODO provjeru da li postoji OIB i da li je aktivan
    #TODO aktiviraj proxy kod pristupanja api-u za provjeru OIB-a - radi statickog ip-a

    #RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"]
    #res = RestClient.get("https://data.poslovna.hr/staging/poslovnaws/wsPoslovna.svc/multicompany?username=cactus&key=cactn1233211bg7410&oib=#{@oib}")
    #puts "Your Static IP is: #{res.body}"

    #params = JSON.load(res)
    #puts params

    #binding.pry

    {'company' => {'oib' => oib, 'name' => "Cactus Code d.o.o.", 'city' => "Zagreb", 'city_id' => '1', 'address' => "Velika cesta 47"}}
  end

  private

  attr_reader :oib

  def user_params
    {
        oib: oib
    }
  end

end