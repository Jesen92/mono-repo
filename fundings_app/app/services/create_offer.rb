class CreateOffer
  def initialize(user, subscription_duration_in_months, unit_price: 100)
    @user = user
    @subscription_duration_in_months = subscription_duration_in_months
    @unit_price = unit_price.to_i
  end

  def create_offer
    offer.save
    offer
    #binding.pry
  end

  private

  attr_accessor :offer, :user
  attr_reader :subscription_duration_in_months, :unit_price

  def offer_count
    user.offers.maximum(:count).to_i + 1
  end

  def reference_number
    user.company.oib + "-" + offer.count.to_s.rjust(5, '0')
  end

  def offer
    @offer ||= Offer.new(
        count: offer_count,
        model: 'HR00',
        description: 'Pretplata za IZI do potpora',
        reference_number: reference_number,
        amount: unit_price.to_i * subscription_duration_in_months.to_i,
        user: user
    )
  end
end