class FundingProgrammeQuery
  def initialize(filter_params: {}, funding_programmes: FundingProgramme.where.not(status: ''))
    @filter_params = filter_params
    @funding_programmes = funding_programmes
  end

  def call
    return funding_programmes if filter_params.blank?

    filter_by_status
    filter_name_and_description
    filter_by_announcer_city_and_county
    filter_by_amounts
    filter_by_dates
    filter_by_programme_type
    filter_by_company_size
    filter_by_programme_city_and_county

    funding_programmes
  end

  private

  attr_accessor :funding_programmes
  attr_reader :filter_params

  def filter_by_status
    self.funding_programmes =
        funding_programmes.where(status: filter_params[:status]) if filter_params[:status]
  end

  def filter_name_and_description
    self.funding_programmes = funding_programmes.where(
      'name ILIKE ? OR description ILIKE ?',
      "%#{filter_params[:name_or_description]}%",
      "%#{filter_params[:name_or_description]}%"
    ) if filter_params[:name_or_description]
  end

  def filter_by_announcer_city_and_county
    self.funding_programmes =
      funding_programmes.where(announcer_city_id: filter_params[:announcer_city_id]) if filter_params[:announcer_city_id]

    self.funding_programmes =
      funding_programmes.where(announcer_county_id: filter_params[:announcer_county_id]) if filter_params[:announcer_county_id]
  end

  def filter_by_amounts
    self.funding_programmes =
      funding_programmes.where('min_amount >= ?', filter_params[:min_amount]) if filter_params[:min_amount]

    self.funding_programmes =
      funding_programmes.where('max_amount <= ?', filter_params[:max_amount]) if filter_params[:max_amount]
  end

  def filter_by_dates
    self.funding_programmes =
        funding_programmes.where('beginning_date >= ?', filter_params[:start_date]) if filter_params[:start_date]

    self.funding_programmes =
        funding_programmes.where('end_date <= ? OR end_term IS NOT NULL', filter_params[:end_date]) if filter_params[:end_date]
  end

  def filter_by_programme_type
    self.funding_programmes =
      funding_programmes.where(programme_type_id: filter_params[:programme_type_id]) if filter_params[:programme_type_id]
  end

  def filter_by_company_size
    self.funding_programmes =
      funding_programmes
      .joins(:eligible_users)
      .where(
        'eligible_users.company_size_id = ?', filter_params[:company_size_id]
      ) if filter_params[:company_size_id]
  end

  def filter_by_programme_city_and_county
    return if filter_params[:programme_city_id].blank? && filter_params[:programme_county_id].blank? && filter_params[:in_croatia].blank?

    self.funding_programmes =
      if filter_params[:in_croatia]
        funding_programmes.where(croatia: true)
      elsif filter_params[:programme_county_id].present?
        funding_programmes.where(
          'croatia = ? OR county_id = ?', true, filter_params[:programme_county_id]
        )
      elsif filter_params[:programme_city_id].present?
        funding_programmes.where(
          'croatia = ? OR city_id = ? OR county_id = ?', true, filter_params[:programme_city_id], programme_county_id_from_city
        )
      else
        funding_programmes
      end
  end

  def programme_county_id_from_city
    @programme_county_id_from_city ||=
      City.find(filter_params[:programme_city_id]).county_id
  end
end
