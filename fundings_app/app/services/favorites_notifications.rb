class FavoritesNotifications
  require 'houston'

  def initialize(funding_programme)
    @funding_programme = funding_programme
  end

  def send_notifications_for_favorites
    users.each do |user|
      @user = user
      send_favorites_via_email if user.email_notifications_flag
      send_favorites_via_ios_notifications if user.ios_notifications_flag
    end
  end

  private

  attr_reader :funding_programme, :user

  ############################################# Email notificaitons
  def send_favorites_via_email
    NotificationMailer.send_favorites_notifications(user, funding_programme).deliver_now
  end
  #############################################

  ############################################# iOS notifications
  def send_favorites_via_ios_notifications
    user.is_development? ? APNDev.push(ios_notification) : APNProd.push(ios_notification)
  end

  def ios_notification
    Houston::Notification.new(
      device: user.ios_push, alert: 'Izmjena na natječaju '+funding_programme.name,
      badge: 0, sound: 'default', custom_data: {programmeId:  funding_programme.id.to_s }
    )
  end
  #############################################

  ############################################# Android notifications
  #TODO android notifications
  #############################################

  def users
    User.where(email_notifications_flag: true).joins(:favorites)
        .where('favorites.funding_programme_id = ?', funding_programme.id)
  end

end