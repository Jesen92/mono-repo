class TaskChoiceSerializer < ActiveModel::Serializer
  attributes :id, :description, :points

  belongs_to :task
end
