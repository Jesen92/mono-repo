class Api::V1::UserPropertiesSerializer < ActiveModel::Serializer
  attributes :id, :email, :phone_number
  attribute :email, if: :email?
  attribute :phone_number, if: :phone_number?

  def email?
    !object.email.blank?
  end

  def phone_number?
    !object.phone_number.blank?
  end
end
