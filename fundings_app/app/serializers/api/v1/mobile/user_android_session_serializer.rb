class Api::V1::Mobile::UserAndroidSessionSerializer < ActiveModel::Serializer
  attributes :email, :android_auth_token
end