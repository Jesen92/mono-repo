class Api::V1::Mobile::UserIosSessionSerializer < ActiveModel::Serializer
  attributes :email, :ios_auth_token
end
