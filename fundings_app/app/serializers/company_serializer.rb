class CompanySerializer < ActiveModel::Serializer
  attributes :name, :oib, :city, :address

  def city
    object.city.name unless object.city.blank?
  end
end
