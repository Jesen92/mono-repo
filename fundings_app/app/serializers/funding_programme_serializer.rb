class FundingProgrammeSerializer < ActiveModel::Serializer
  attributes :id, :name, :status ,:beginning_date, :end_date, :cofinancing_percentage, :min_amount, :max_amount,
             :to_cofinancing, :end_term

  attribute :error, if: :errors_condition?
#=begin
  belongs_to :purpose
  belongs_to :programme_type
  belongs_to :county
  belongs_to :city, serializer: CitySerializer
  belongs_to :announcer_county
  belongs_to :announcer_city
  has_many :company_sizes
#=end

  def errors_condition?
    object.errors.empty? ? false : true
  end

end
