class PurposeSerializer < ActiveModel::Serializer
  include AssetsEnvironmentHelper
  attributes :id
  attribute :url_medium, if: :file_exists?
  attribute :url_thumb, if: :file_exists?

  def url_medium
    URI.join(environment_assets, object.avatar.url(:medium)).to_s if environment_assets?
  end

  def url_thumb
    URI.join(environment_assets, object.avatar.url(:thumb)).to_s if environment_assets?
  end

  def file_exists?
    object.avatar.file?
  end

end
