class TaskPhaseSerializer < ActiveModel::Serializer
  attributes :id, :name, :max_points, :min_points
  attribute :task_groups, if: :task_groups?
  attribute :tasks, if: :tasks?


  def task_groups
    object.task_groups.map do |group|
      TaskGroupSerializer.new(group)
    end
  end

  def task_groups?
    !object.task_groups.empty?
  end

  def tasks
    object.tasks.map do |task|
      TaskSerializer.new(task)
    end
  end

  def tasks?
    !object.tasks.empty?
  end

end
