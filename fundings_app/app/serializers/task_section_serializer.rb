class TaskSectionSerializer < ActiveModel::Serializer
  attributes :id, :name, :min_points, :max_points
  has_many :tasks, serializer: TaskSerializer, if: :yes_no_tasks?
  has_many :task_phases, serializer: TaskPhaseSerializer, if: :task_phases?

  def yes_no_tasks?
    !object.tasks.empty?
  end

  def task_phases?
    !object.task_phases.empty?
  end
end
