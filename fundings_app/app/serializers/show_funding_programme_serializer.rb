class ShowFundingProgrammeSerializer < ActiveModel::Serializer
  attributes :id, :name, :beginning_date, :end_date, :cofinancing_percentage, :to_cofinancing,
             :min_amount, :max_amount, :description, :funding_programme_link, :croatia

  belongs_to :purpose
  belongs_to :programme_type
  belongs_to :county
  belongs_to :city, serializer: CitySerializer
  has_many :programme_documents, serializer: ProgrammeDocumentsSerializer
  has_many :company_sizes

  belongs_to :announcer_county
  belongs_to :announcer_city

end
