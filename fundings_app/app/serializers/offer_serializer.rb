class OfferSerializer < ActiveModel::Serializer
  attributes :id, :model, :reference_number, :amount, :description, :created_at
  attribute :company

  def created_at
    object.created_at.strftime("%d.%m.%Y.")
  end

  def company
    CompanySerializer.new(object.user.company)
  end
end
