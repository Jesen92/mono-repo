class ProgrammeDocumentsSerializer < ActiveModel::Serializer
  include AssetsEnvironmentHelper
  attributes :id, :document_file_name, :document_content_type, :document_file_size, :document_updated_at, :url

  def url
    #ActionController::Base.asset_host+ object.document.url(:original)
    URI.join(environment_assets, object.document.url(:original)).to_s if environment_assets?
  end
end
