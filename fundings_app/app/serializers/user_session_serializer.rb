class UserSessionSerializer < ActiveModel::Serializer
  attributes :email, :auth_token
  attribute :errors, if: :errors_condition?

  def errors_condition?
    object.errors.empty? ? false : true
  end
end
