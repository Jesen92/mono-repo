class TaskGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :max_points

  has_many :tasks
end
