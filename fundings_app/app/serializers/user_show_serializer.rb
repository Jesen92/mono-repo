class UserShowSerializer < ActiveModel::Serializer
  attributes :id, :email, :phone_number, :mobile_app

  has_one :company

  def mobile_app
    !object.android_auth_token.blank? || !object.ios_auth_token.blank?
  end
end
