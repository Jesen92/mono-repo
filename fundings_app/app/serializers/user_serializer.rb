class UserSerializer < ActiveModel::Serializer
  attributes :id, :auth_token, :ios_auth_token, :android_auth_token
  attribute :errors, if: :errors_condition?
  attribute :email, if: :email?
  attribute :phone_number, if: :phone_number?

  def email?
    !object.email.blank?
  end

  def phone_number?
    !object.phone_number.blank?
  end

  def errors_condition?
    object.errors.empty? ? false : true
  end
end
