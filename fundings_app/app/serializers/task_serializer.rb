class TaskSerializer < ActiveModel::Serializer
  attributes :id, :description, :yes_no
  attribute :task_choices, if: :task_choice?

  def task_choice?
    !object.task_choices.empty?
  end
end
