class YesNoTaskSerializer < ActiveModel::Serializer
  attributes :id, :description
end
