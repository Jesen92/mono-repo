module AssetsEnvironmentHelper
  def environment_assets?
    ENV['ENVIRONMENT_ASSETS_PIPELINE'].present?
  end

  def environment_assets
    ENV['ENVIRONMENT_ASSETS_PIPELINE']
  end
end
