class FundingProgrammesController < ApplicationController
  require 'rest-client'
  before_action :set_funding_programme, only: [:show, :edit, :update, :destroy]

  # GET /funding_programmes
  # GET /funding_programmes.json
  def index
    @funding_programmes = FundingProgramme.all

    @funding_programmes_grid = initialize_grid(@funding_programmes, name: 'Programi_sufinanciranja',
                                               :include => [:purpose, :programme_type, :city, :county],
                                               order: 'funding_programmes.created_at',
                                               order_direction: 'desc')

  end

  # GET /funding_programmes/1
  # GET /funding_programmes/1.json
  def show
  end

  # GET /funding_programmes/new
  def new
    @funding_programme = FundingProgramme.new
    gon.mycounter, gon.billcounter,
        gon.phasecounter, gon.groupcounter = 1, 1, 1, 1
  end

  # GET /funding_programmes/1/edit
  def edit
    gon.mycounter, gon.billcounter,
        gon.phasecounter, gon.groupcounter = 1, 1, 1, 1
  end

  # POST /funding_programmes
  # POST /funding_programmes.json
  def create
    if params[:create_from_existing]
      @funding_programme = clone_funding_programme(params[:id])
    else
      @funding_programme = FundingProgramme.new(funding_programme_params)
    end

    respond_to do |format|
      if @funding_programme.save

        if params[:documents]
          params[:documents].each { |document|
            @funding_programme.programme_documents.create(document: document)
          }
        end

        format.html { redirect_to @funding_programme, notice: 'Funding programme was successfully created.' }
        format.json { render :show, status: :created, location: @funding_programme }
      else
        format.html { render :new }
        format.json { render json: @funding_programme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /funding_programmes/1
  # PATCH/PUT /funding_programmes/1.json
  def update

    @funding_programme ||= @funding_programme if params[:create_edit]

    respond_to do |format|
      if @funding_programme.update_attributes(funding_programme_params)

        if params[:documents]
          params[:documents].each { |document|
            @funding_programme.programme_documents.create(document: document)
          }
        end

        format.html { redirect_to @funding_programme, notice: 'Funding programme was successfully updated.' }
        format.json { render :show, status: :ok, location: @funding_programme }
      else
        format.html { render :edit }
        format.json { render json: @funding_programme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /funding_programmes/1
  # DELETE /funding_programmes/1.json
  def destroy
    @funding_programme.destroy
    respond_to do |format|
      format.html { redirect_to funding_programmes_url, notice: 'Funding programme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_document
    ProgrammeDocument.find(params[:id]).destroy
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_funding_programme
      @funding_programme = FundingProgramme.find(params[:id])
    end

    def clone_funding_programme(id)
      FundingProgramme.find_by(id: id).deep_clone include:
                                                               [task_sections:
                                                                    {tasks: [:task_choices],
                                                                     task_phases: {tasks: [:task_choices],task_groups: {tasks: [:task_choices]
                                                                     }}
                                                                    }], except: :status
      #TODO neka se i datoteke dupliciraju
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def funding_programme_params
      params.require(:funding_programme).permit(:croatia ,:status, :name, :description, :beginning_date, :end_date, :end_term, :min_points,
                                                :cofinancing_percentage, :to_cofinancing, :purpose_id, :programme_type_id,
                                                :city_id, :county_id, :announcer_croatia, :announcer_city_id, :announcer_county_id,
                                                :min_amount, :max_amount, :funding_programme_link,
                                                :company_size_ids => [], task_sections_attributes: task_sections_attributes)
    end

  def task_sections_attributes
    [
      :id, :radio_button,:name, :min_points, :description, :_destroy,
      tasks_attributes: tasks_attributes,
      task_phases_attributes: task_phases_attributes
    ]
  end

  def task_phases_attributes
    [
        :id, :name, :max_points, :min_points, :_destroy,
        task_groups_attributes: task_groups_attributes,
        tasks_attributes: tasks_attributes
    ]
  end

  def task_groups_attributes
    [
        :id, :max_points, :name, :_destroy,
        tasks_attributes: tasks_attributes
    ]
  end

  def tasks_attributes
    [
        :id, :description, :yes_no, :_destroy,
        task_choices_attributes: task_choices_attributes
    ]
  end

  def task_choices_attributes
    [
        :id, :description, :points, :_destroy
    ]
  end

  def programme_documents_attributes
    [
        :content
    ]
  end

end
