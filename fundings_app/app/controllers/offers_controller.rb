class OffersController < ApplicationController
  before_action :set_offer, only: :show

  def show
  end

  def index
    @offers = Offer.all

    @offers_grid = initialize_grid(@offers, name: 'Ponude',
                                               :include => [{user: :company}],
                                               order: 'offers.created_at',
                                               order_direction: 'desc')

  end

  private

  def set_offer
    @offer = Offer.find(params[:id])
  end
end
