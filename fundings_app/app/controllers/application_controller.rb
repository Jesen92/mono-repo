class ApplicationController < ActionController::Base
  before_action :authenticate_admin!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # AuthorizedController
=begin
  def current_company
    return unless current_user_signed_in?

    @current_company ||= current_user.company
  end
=end
end
