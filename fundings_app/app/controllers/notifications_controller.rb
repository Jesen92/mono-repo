class NotificationsController < ApplicationController
  before_action :set_funding_programme

  def create

    #(respond_with_notifications_error and return) if ((DateTime.now+5.minutes).to_time - @funding_programme.notification_sent_at.to_time)/1.minutes > 5

    FavoritesNotifications.new(@funding_programme).send_notifications_for_favorites

    flash[:notice] = "Notifikacije uspješno poslane!"
    @funding_programme.update(notification_sent_at: DateTime.now)
    redirect_to :back
  end

  private

  def set_funding_programme
    @funding_programme = FundingProgramme.find_by(id: params[:id])

    #if @funding_programme.notifications_sent_at < DateTime.now - Date
  end

  def respond_with_notifications_error
    flash[:notice] = "Notifikacija je poslana prije manje od 5 minuta!"
    redirect_to :back
  end

end
