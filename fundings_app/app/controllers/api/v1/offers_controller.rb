module Api
  module V1
    class OffersController < AuthorizedController
      before_action :authenticate_user_from_auth_token!

      def create
        offer = CreateOffer.new(current_user, create_offer_params[:multiplier]).create_offer

        render json: offer, serializer: OfferSerializer
      end

      private

      def create_offer_params
        params.require(:offer).permit(
          :multiplier
        )
      end

    end
  end
end

