module Api
  module V1
    class NotificationsController < AuthorizedController
      before_action :authenticate_user_from_auth_token!

      def set_flag
        if current_user.update(set_flag_params)
          render json: { "notice" => "Uspješno postavljena zastavica za notifikacije!"}.to_json
        else
          current_user.errors.add(:id, "Pogreška kod spremanja flag-a za notifikacije!")
          render json: current_user
        end
      end

      private

      def set_flag_params
        params.require(:user).permit(
          :email_notifications_flag, :is_development
        )
      end
    end
  end
end

