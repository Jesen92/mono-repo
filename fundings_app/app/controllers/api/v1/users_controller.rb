module Api
  module V1
    class UsersController < AuthorizedController
      skip_before_action :authenticate_user_from_auth_token!, only: [:create, :check_oib, :reset_password]

      def check_oib
        company = CheckOibApi.new(company_params).check_oib

        (respond_with_oib_error and return) if company.blank?
        render json: company
      end

      def show
        user = User.find_by(email: user_show_params[:email])

        (respond_with_email_error and return) if user .blank?

        render json: user , serializer: UserShowSerializer
      end

      def create
        (respond_with_registration_error and return) if user_params[:email] && user_params[:phone_number]

        company = Company.find_by(oib: user_params[:company_attributes][:oib])
        user = User.new(user_params)

        if company
          return render json: { "user" => {"errors" => {"id" => ["Poduzetnik već postoji u sustavu!"]}}}.to_json, status: :bad_request
        else
          #Ako kompanija ne postoji tu će probati save-at
          return render json: { "user" => {"errors" => {"id" => ["Korisnik sa upisanom e-mail adresom ili brojem mobitela već postoji!"]}}}.to_json, status: :bad_request unless user.save
        end

        render json: user
      end

      def update
        user = User.find(params[:id])

        user.update(user_params)

        render json: user
      end

      def destroy
        user = User.find_by(email: user_params[:email])

        user.destroy

        render json: user
      end

      def reset_password
        user = User.find_by(email: user_params[:email])

        password_reset(user)
      end

      private

      def user_params
        params.require(:user).permit(
            :phone_number, :email, :password, :password_confirmation, company_attributes: [ :name, :oib, :address, :city_id, :company_size_id]
        )
      end

      def company_params
        params.require(:company).permit(
            :oib
        )
      end

      def user_show_params
        params.require(:user).permit(
          :email
        )
      end

      def password_reset(user)
        return render json: { "errors" => { "id" => ["Korisnik sa upisanim email-om ne postoji!"]}}.to_json, status: :bad_request unless user

        if !user.reset_password_sent_at || user.reset_password_sent_at < 1.day.ago
          user.send_reset_password_instructions
          render json: { "user" => "Poslan je email sa instrukcijama za resetiranje lozinke"}.to_json
        else
          render json: { "errors" => { "id" => ["Lozinka se može resetirati samo 1 dnevno!"]}}.to_json, status: :bad_request
        end
      end

      #Error message
      def oib_errors
        @auth_error_message ||= 'Pogrešno upisani OIB! Napomena: Korisnik se smije samo jednom sa istim OIB-om registrirati te kompanija mora biti aktivna!'
        { id:[ @auth_error_message] }
      end

      def respond_with_oib_error
        render json: { errors: oib_errors }, status: :bad_request
      end

      ######################### Email error message
      def email_errors
        @auth_error_message ||= 'Nije pronađen korisnik po danom email-u!'
        { id:[ @auth_error_message] }
      end

      def respond_with_email_error
        render json: { errors: email_errors }, status: :bad_request
      end
      #########################

      ######################### Registration error message
      def registration_errors
        @auth_error_message ||= 'Nepravilan e-mail ili broj mobitela!'
        { id:[ @auth_error_message] }
      end

      def respond_with_registration_error
        render json: { errors: registration_errors }, status: :bad_request
      end
      #########################


    end
  end
end

