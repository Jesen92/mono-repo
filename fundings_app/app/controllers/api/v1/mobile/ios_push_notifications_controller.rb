module Api
  module V1
    module Mobile
      class IosPushNotificationsController < AuthorizedController
        before_action :authenticate_user_from_auth_token!

        def add_push_id_to_user
          puts "Usao"
          if current_user.update(push_id_params)
            render json: { "notice" => "Uspješno postavljene notifikacije!"}.to_json
          else
            current_user.errors.add(:id, "Pogreška kod spremanja id-a za notifikacije!")
            render json: current_user, :status => :bad_request
          end
        end

        def set_flag
          if current_user.update(push_flag_params)
            render json: { "notice" => "Uspješno postavljena zastavica za notifikacije!"}.to_json
          else
            current_user.errors.add(:id, "Pogreška kod spremanja flag-a za notifikacije!")
            render json: current_user
          end
        end

        private

        def push_id_params
          params.require(:user).permit(
            :ios_push, :ios_notifications_flag
          )
        end

        def push_flag_params
          params.require(:user).permit(
              :ios_notifications_flag
          )
        end

      end
    end
  end
end

