module Api
  module V1
    module Mobile
      class FundingsController < AuthorizedController
        before_action :authenticate_user_from_auth_token!

        def index
          puts "Output je: "+fundings_page_params[:page]+", "+fundings_page_params[:per_page]
          funding_programmes = FundingProgramme.where.not(status: '').order(:created_at).page(fundings_page_params[:page]).per(fundings_page_params[:per_page])

          render json: funding_programmes
        end

        def show
          puts "ID je: "+params[:id].to_s
          funding_programme = FundingProgramme.find_by(id: fundings_programme_show[:id])

          render json: funding_programme, serializer: ShowFundingProgrammeSerializer
        end

        private

        def fundings_page_params
          params.require(:funding_programmes).permit(
              :page, :per_page
          )
        end

        def fundings_programme_show
          params.require(:funding_programmes).permit(
            :id
          )
        end
      end
    end
  end
end

