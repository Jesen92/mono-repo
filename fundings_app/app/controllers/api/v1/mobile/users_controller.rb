module Api
  module V1
    module Mobile
      class UsersController < AuthorizedController
        skip_before_action :authenticate_user_from_auth_token!, only: [:create, :check_oib, :reset_password]


        def check_oib
          company = CheckOibApi.new(company_params).check_oib

          (respond_with_oib_error and return) if company.blank?
          render json: company
        end

        #def index
        #  # paginate je ovdje helper metoda, ako ce ti trebat paginacija koristi kaminarijev API za nju
        #  users = paginate(User.all)
        #
        #  respond_with users, each_serializer: Api::V1::UserSerializer
        #end

        def show
          user = User.find_by(email: user_show_params[:email])

          (respond_with_email_error and return) if user .blank?

          render json: user , serializer: UserShowSerializer
        end

        def create
          company = Company.find_by(oib: user_params[:company_attributes][:oib])
          user = User.new(user_params)

          if company
            user.errors.add(:id, 'Kompanija već postoji!')
          else
            #Ako kompanija ne postoji tu će probati save-at
            user.errors.add(:id, 'Korisnik sa upisanom email adresom već postoji') unless user.save
          end

          render json: user
        end

        def create_with_phone_number_confirmation
          company = Company.find_by(oib: user_params[:company_attributes][:oib])
          user = User.new(user_params)
          user.skip_confirmation!

          if company
            return render json: { "user" => {"errors" => {"id" => ["Poduzetnik već postoji u sustavu!"]}}}.to_json, status: :bad_request
          else
            #Ako kompanija ne postoji tu će probati save-at
            return render json: { "user" => {"errors" => {"id" => ["Korisnik sa upisanom e-mail adresom ili brojem mobitela već postoji!"]}}}.to_json, status: :bad_request unless user.save
          end

          render json: user
        end

        def update
          user = User.find(params[:id])

          user.update(user_params)

          render json: user
        end

        def destroy
          user = User.find_by(email: user_params[:email])

          user.destroy

          render json: user
        end

        def reset_password
          user = User.find_by(email: user_params[:email])

          password_reset(user)
        end

        private

        def user_params
          params.require(:user).permit(
              :email, :password, :password_confirmation, company_attributes: [ :name, :oib, :address, :city_id, :company_size_id]
          )
        end

        def company_params
          params.require(:company).permit(
              :oib
          )
        end

        def password_reset(user)
          return render json: { "errors" => { "id" => ["Korisnik sa upisanim email-om ne postoji!"]}}.to_json, status: :bad_request unless user

          if !user.reset_password_sent_at || user.reset_password_sent_at < 1.day.ago
            user.send_reset_password_instructions
            render json: { "user" => "Poslan je email sa instrukcijama za resetiranje lozinke"}.to_json
          else
            render json: { "errors" => { "id" => ["Lozinka se može resetirati samo 1 dnevno!"]}}.to_json, status: :bad_request
          end
        end

        #Error message
        def oib_errors
          @auth_error_message ||= 'Pogrešno upisani OIB! Napomena: Korisnik se smije samo jednom sa istim OIB-om registrirati te kompanija mora biti aktivna!'
          { id:[ @auth_error_message] }
        end

        def respond_with_oib_error
          render json: { errors: oib_errors }, status: :bad_request
        end

        ######################### Email error message
        def email_errors
          @auth_error_message ||= 'Nije pronađen korisnik po danom email-u!'
          { id:[ @auth_error_message] }
        end

        def respond_with_email_error
          render json: { errors: email_errors }, status: :bad_request
        end
        #########################

      end
    end
  end
end

