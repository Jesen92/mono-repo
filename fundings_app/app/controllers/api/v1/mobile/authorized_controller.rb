module Api
  module V1
    module Mobile
      class AuthorizedController < ApiController
        include Devise::Controllers::Helpers

        before_action :authenticate_user_from_auth_token!

        private

        # Autorizacija se odradjuje preko headera:
        # Authorization1: Token token=token-string, identifier=user-email@gmail.com
        def authenticate_user_from_auth_token!
          user = authenticate_with_http_token do |token, options|
            user = User.find_by(email: options['identifier']) || User.find_by(phone_number: options['identifier'])

            if options['platform'].include? 'android'
              user if Devise.secure_compare(token, user.android_auth_token)
            else
              user if Devise.secure_compare(token, user.ios_auth_token)
            end
          end

          (respond_with_auth_error and return) if user.blank?
          (respond_with_not_confirmed_error and return) if user.confirmed_at.blank? #Korisnik nije potvrdio account
          (respond_with_payment_required_error and return) if user.company.license_expires.present? && user.company.license_expires > Date.today #Korisnik nije uplatio za korištenje usluge

          # devise, daje ti current_user metodu
          sign_in(user, store: false)
        end

        ######################## Sign in error
        def auth_errors
          @auth_error_message ||= 'Molimo ulogirajte se da bi se zadana naredba izvršila!'
          { id:[ @auth_error_message], detail: 'not authenticated' }
        end

        def respond_with_auth_error
          render json: { errors: auth_errors }, status: :unauthorized
        end
        ########################

        ######################## User not confirmed error
        def not_confirmed_errors
          @auth_error_message ||= 'Molimo potvrdite svoj korisnički račun kako bi mogli koristiti usluge aplikacije!'
          { id:[ @auth_error_message], detail: 'not confirmed' }
        end

        def respond_with_not_confirmed_error
          render json: { errors: not_confirmed_errors }, status: :forbidden
        end
        ########################

        ######################## User not paid
        def payment_required_errors
          @auth_error_message ||= 'Za korištenje pune usluge aplikacije morate biti preplaćeni na usluge!'
          { id:[ @auth_error_message], detail: 'payment Required' }
        end

        def respond_with_payment_required_error
          render json: { errors: payment_required_errors }, status: :payment_required
        end
        ########################
      end
     end
  end
end
