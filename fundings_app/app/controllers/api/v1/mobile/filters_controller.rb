module Api
  module V1
    module Mobile
      class FiltersController < FundingsController
        skip_before_action :authenticate_user_from_auth_token!, only: :show

        def show
          #render nothing: true
          filters_params = { :company_sizes => CompanySize.all, :programme_types => ProgrammeType.all, :cities => City.all, :counties => County.all}

          render json: filters_params
        end

        def create
          query = FundingProgrammeQuery.new(filter_params: filter_params)

          results = query.call

          render json: results
        end

        private

        def filter_params
          params.require(:filters).permit(
              :status,
              :name_or_description,
              :company_size_id,
              :programme_type_id,
              :min_amount,
              :max_amount,
              :end_date,
              :announcer_city_id,
              :announcer_county_id,
              :programme_city_id,
              :programme_county_id,
              :croatia_wide,
              :start_date
          )
        end
      end
    end
  end
end
