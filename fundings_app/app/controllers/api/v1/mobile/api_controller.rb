module Api
  module V1
    module Mobile
      class ApiController < ApplicationController
        skip_before_action :authenticate_admin!, :verify_authenticity_token
      end
    end
  end
end