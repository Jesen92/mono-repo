module Api
  module V1
    class TaskController < AuthorizedController
      before_action :authenticate_user_from_auth_token!

      def show
        task_section = TaskSection.where(funding_programme_id: section_params[:id]).order(:id)

        render json: task_section
      end

      def create
      end

      def update
      end

      private

      def section_params
        params.require(:funding_programme).permit(
            :id
        )
      end

    end
  end
end
