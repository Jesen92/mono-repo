module Api
  module V1
    class ApiController < ApplicationController
      skip_before_action :authenticate_admin!, :verify_authenticity_token
      before_action :configure_permitted_parameters, if: :devise_controller?

      protected

      def configure_permitted_parameters
        added_attrs = [:phone_number, :email, :password, :password_confirmation, :remember_me]
        devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
        devise_parameter_sanitizer.permit :account_update, keys: added_attrs
      end
    end
  end
end