module Api
  module V1
    class ConfirmationsController < Devise::ConfirmationsController
      private
      def after_confirmation_path_for(resource_name, resource)
        #TODO dodaj putanju na koju će otići nakon potvrđivanje email-a
        #TODO postavi u routes.rb devise confirmation controller na OVAJ controller
        super
      end
    end
  end
end