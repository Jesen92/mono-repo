module Api
  module V1
    class FavoritesController < AuthorizedController
      before_action :authenticate_user_from_auth_token!

      def create
        @favorite = current_user.favorites.build(favorites_params)

        if @favorite.save
          current_user.favorites << @favorite
          render json: { 'notice' => 'Dodan je novi favorit!'}
        else
          @favorite.errors.add(:id, 'Pogreška kod dodavanja favorita!')
          render json: @favorite #TODO postavi error i status 400
        end
      end

      def show
        favorites = FundingProgramme.where(id: current_user.favorites.map { |favorite| favorite.funding_programme_id })

        (respond_with_favorites_error and return) if favorites.empty?

        render json: favorites
      end

      def destroy
        favorite = Favorite.find_by(user_id: current_user.id, funding_programme_id: favorites_params[:funding_programme_id])

        (respond_with_favorite_not_found_error and return) if favorite.blank?

        favorite.destroy
        render json: { 'notice' => 'Favorit je uspješno maknut!'}
      end

      private

      def favorites_params
        params.require(:favorites).permit(
          :funding_programme_id
        )
      end

      #Error message for no favorites
      def no_favorites_errors
        @favorites_error_message ||= 'Ne postoji ni jedan favorit!'
        { id:[ @favorites_error_message] }
      end

      def respond_with_favorites_error
        render json: { errors: no_favorites_errors }, status: :bad_request
      end

      def no_favorite_errors
        @favorites_error_message ||= 'Favorit ne postoji!!'
        { id:[ @favorites_error_message] }
      end

      def respond_with_favorite_not_found_error
        render json: { errors: no_favorite_errors }, status: :bad_request
      end
    end
  end
end

