class TaskSection < ActiveRecord::Base
  #Faza
  belongs_to :funding_programme

  has_many :tasks, inverse_of: :task_section
  accepts_nested_attributes_for :tasks, reject_if: :all_blank, allow_destroy: true

  has_many :task_phases, inverse_of: :task_section
  accepts_nested_attributes_for :task_phases, reject_if: :all_blank, allow_destroy: true
end
