class Favorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :funding_programme
end
