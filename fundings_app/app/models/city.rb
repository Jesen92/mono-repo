class City < ActiveRecord::Base
  belongs_to :county

  has_many :companies, :foreign_key => "city_id"
end
