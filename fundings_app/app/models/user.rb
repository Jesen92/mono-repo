class User < ActiveRecord::Base
  MAX_PHONE_ACTIVATION_TOKEN = 999_999

  belongs_to :company
  accepts_nested_attributes_for :company, reject_if: :all_blank, allow_destroy: true

  has_many :favorites
  has_many :funding_programmes, :through => :favorites

  has_many :offers

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :authentication_keys => [:email]

  validates :auth_token, presence: true, uniqueness: true

  validates :android_auth_token, presence: true, uniqueness: true
  validates :ios_auth_token, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true, allow_nil: true, unless: 'email.blank?'


  before_validation :generate_auth_token, if: 'auth_token.blank?'
  before_validation :generate_android_auth_token, if: 'android_auth_token.blank?'
  before_validation :generate_ios_auth_token, if: 'ios_auth_token.blank?'

  validates :phone_number, format: /\A[1-9]{1}[0-9]{8}\z/, uniqueness: true, allow_nil: true
  validates :phone_number, presence: true, if: 'email.blank?'

  def regenerate_auth_token!
    generate_auth_token
    save
  end

  def regenerate_android_auth_token!
    generate_android_auth_token
    save
  end

  def regenerate_ios_auth_token!
    generate_ios_auth_token
    save
  end

  private

  def generate_auth_token
    loop do
      self.auth_token = Devise.friendly_token
      break unless User.exists?(auth_token: auth_token)
    end
  end

  def generate_android_auth_token
    loop do
      self.android_auth_token = Devise.friendly_token
      break unless User.exists?(android_auth_token: auth_token)
    end
  end

  def generate_ios_auth_token
    loop do
      self.ios_auth_token = Devise.friendly_token
      break unless User.exists?(ios_auth_token: auth_token)
    end
  end

  # Require email only if phone number is not present
  def email_required?
    phone_number.blank?
  end

  def email_changed?
    phone_number.blank?
  end

  # Use devise methods to handle phone token generation
  def generate_confirmation_token
    super
    #binding.pry
    if phone_number.present?
      self.confirmation_token = @raw_confirmation_token =
        SecureRandom.random_number(MAX_PHONE_ACTIVATION_TOKEN).to_s.ljust(6, '0')
    end
  end

  # Use devise methods to handle sending confirmation instructions
  def send_confirmation_instructions
    (super && return) if phone_number.blank?
    binding.pry

    logger.info "[USER] Sending SMS message"

    # Twilio::Rest::Client.new.messages.create(
    #   from: '+385',
    #   to: '+385' + phone_number,
    #   body: "Vas aktivacijski kod je: #{phone_activation_token}"
    # )
  end

end
