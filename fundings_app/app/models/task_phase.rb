class TaskPhase < ActiveRecord::Base
  #Sekcija
  has_many :task_groups
  accepts_nested_attributes_for :task_groups, reject_if: :all_blank, allow_destroy: true

  belongs_to :task_section

  has_many :tasks
  accepts_nested_attributes_for :tasks, reject_if: :all_blank, allow_destroy: true
end
