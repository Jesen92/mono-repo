class EligibleUser < ActiveRecord::Base
  belongs_to :company_size
  belongs_to :funding_programme
end
