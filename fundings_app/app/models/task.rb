class Task < ActiveRecord::Base
  belongs_to :task_section
  belongs_to :task_group

  has_many :task_choices, inverse_of: :task
  accepts_nested_attributes_for :task_choices, reject_if: :all_blank, allow_destroy: true
end
