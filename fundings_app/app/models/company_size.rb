class CompanySize < ActiveRecord::Base
  has_many :companies

  has_many :eligible_users #, :dependent => :destroy
  has_many :funding_programmes, :through => :eligible_users
end
