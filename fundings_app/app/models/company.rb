class Company < ActiveRecord::Base

  validates :oib, presence: true, uniqueness: true

  has_one :user
  belongs_to :company_size
  belongs_to :city

  after_create {
    if self.city.county_id == 21
      self.license_expires = Date.today+1.year
      self.save
    end
  }

end
