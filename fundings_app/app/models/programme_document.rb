class ProgrammeDocument < ActiveRecord::Base
  belongs_to :funding_programme

  has_attached_file :document,
                    :path => "/izi/documents/:id/:filename",
                    :url  => "/documents/:id/:filename"

  do_not_validate_attachment_file_type :document
end
