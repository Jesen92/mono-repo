class FundingProgramme < ActiveRecord::Base
  belongs_to :purpose
  belongs_to :programme_type
  belongs_to :county
  belongs_to :city

  belongs_to :announcer_county, :foreign_key => "announcer_county_id", :class_name => "County"
  belongs_to :announcer_city, :foreign_key => "announcer_city_id", :class_name => "City"

  has_many :eligible_users #, :dependent => :destroy
  has_many :company_sizes, :through => :eligible_users
  accepts_nested_attributes_for :company_sizes

  has_many :programme_documents, :dependent => :destroy
  accepts_nested_attributes_for :programme_documents, reject_if: :all_blank

  has_many :task_sections, inverse_of: :funding_programme
  accepts_nested_attributes_for :task_sections, reject_if: :all_blank, allow_destroy: true

  has_many :favorites #, :dependent => :destroy
  has_many :users, :through => :favorites
end
