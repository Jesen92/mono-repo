class Purpose < ActiveRecord::Base
  has_many :funding_programmes

  has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "/images/missing_thumb.jpg"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end
