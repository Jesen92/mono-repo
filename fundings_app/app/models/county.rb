class County < ActiveRecord::Base
  has_many :funding_programmes
  has_many :cities
end
