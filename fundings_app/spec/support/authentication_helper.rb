module AuthenticationHelper
  def sign_in user
    request.headers['Authorization'] = "Token token=#{user.auth_token}, email=#{user.email}"
  end

  def sign_out
    request.headers['Authorization'] = nil
  end
end
