FactoryGirl.define do
  factory :company do
    sequence(:name) { |n| "Company #{n}" }
    oib '2131232131231'
  end
end
