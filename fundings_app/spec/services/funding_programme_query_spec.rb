require 'rails_helper'

describe FundingProgrammeQuery do
  subject { FundingProgrammeQuery.new(filter_params: filter_params) }

  describe '#call' do
    context 'when sending name_or_description param' do
      let!(:funding_programme_1) { create(:funding_programme, name: 'Cactus Code') }
      let!(:funding_programme_2) { create(:funding_programme, description: 'Svastaraj') }

      context 'when name matches' do
        let(:filter_params) { { name_or_description: 'Code' } }

        it 'should return the correct funding programme' do
          expect(subject.call.pluck(:id))
            .to match_array(Array(funding_programme_1.id))
        end
      end

      context 'when name matches' do
        let(:filter_params) { { name_or_description: 'stara' } }

        it 'should return the correct funding programme' do
          expect(subject.call.pluck(:id))
            .to match_array(Array(funding_programme_2.id))
        end
      end
    end

    context 'when sending announcer_city_id param' do
      let!(:funding_programme_1) { create(:funding_programme, announcer_city_id: 1) }
      let!(:funding_programme_2) { create(:funding_programme, announcer_city_id: 2) }

      let(:filter_params) { { announcer_city_id: 2 } }

      it 'should return the correct funding programme' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_2.id))
      end
    end

    context 'when sending announcer_county_id param' do
      let!(:funding_programme_1) { create(:funding_programme, announcer_county_id: 1) }
      let!(:funding_programme_2) { create(:funding_programme, announcer_county_id: 2) }

      let(:filter_params) { { announcer_county_id: 2 } }

      it 'should return the correct funding programme' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_2.id))
      end
    end

    context 'when sending min_amount' do
      let!(:funding_programme_1) { create(:funding_programme, min_amount: 100) }
      let!(:funding_programme_2) { create(:funding_programme, min_amount: 200) }

      let(:filter_params) { { min_amount: 150 } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_2.id))
      end
    end

    context 'when sending min_amount' do
      let!(:funding_programme_1) { create(:funding_programme, max_amount: 100) }
      let!(:funding_programme_2) { create(:funding_programme, max_amount: 200) }

      let(:filter_params) { { max_amount: 150 } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_1.id))
      end
    end

    context 'when sending programme type id' do
      let!(:funding_programme_1) { create(:funding_programme, programme_type_id: 1) }
      let!(:funding_programme_2) { create(:funding_programme, programme_type_id: 2) }

      let(:filter_params) { { programme_type_id: 2 } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_2.id))
      end
    end

    context 'when sending programme type id' do
      let!(:funding_programme_1) { create(:funding_programme) }
      let!(:funding_programme_2) { create(:funding_programme) }
      let!(:eligible_user_1) { create(:eligible_user, funding_programme_id: funding_programme_1.id, company_size_id: 10) }
      let!(:eligible_user_2) { create(:eligible_user, funding_programme_id: funding_programme_2.id, company_size_id: 15) }

      let(:filter_params) { { company_size_id: 15 } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_2.id))
      end
    end

    context 'when filtering by whole country' do
      let!(:funding_programme_1) { create(:funding_programme, croatia: true, county: County.find_by(name: 'Krapinsko-zagorska')) }
      let!(:funding_programme_2) { create(:funding_programme, county: County.find_by(name: 'Primorsko-goranska')) }

      let(:filter_params) { { in_croatia: true } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_1.id))
      end
    end

    context 'when filtering by programme county' do
      let!(:funding_programme_1) { create(:funding_programme, county: County.find_by(name: 'Krapinsko-zagorska')) }
      let!(:funding_programme_2) { create(:funding_programme, county: County.find_by(name: 'Primorsko-goranska')) }

      let(:filter_params) { { programme_county_id: County.find_by(name: 'Krapinsko-zagorska').id } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array(Array(funding_programme_1.id))
      end
    end

    context 'when filtering by programme city' do
      let!(:funding_programme_1) { create(:funding_programme, city: City.find_by(name: 'Samobor')) }
      let!(:funding_programme_2) { create(:funding_programme, county: County.find_by(name: 'Zagrebačka')) }
      let!(:funding_programme_3) { create(:funding_programme, city: City.find_by(name: 'Rijeka')) }

      let(:filter_params) { { programme_city_id: City.find_by(name: 'Samobor').id } }

      it 'should return the correct funding program' do
        expect(subject.call.pluck(:id))
          .to match_array([funding_programme_1.id, funding_programme_2.id])
      end
    end
  end
end
