require 'rails_helper'

describe Api::V1::FiltersController do
  let(:user) { create(:user) }

  before { sign_in user }

  describe 'GET #show' do
    it 'should return success' do
      xhr :get, :show

      expect(response.status).to eq(200)
    end
  end

  describe 'POST #create' do
    it 'should return success' do
      xhr :post, :create, filters: {}

      expect(response.status).to eq(200)
    end

    let!(:eligible_user) do
      create(:eligible_user,
             funding_programme_id: funding_programme.id,
             company_size_id: 5)
    end

    let!(:funding_programme) do
      create(
        :funding_programme,
        name: 'Cactus Code',
        announcer_city_id: 1,
        min_amount: 10_000,
        max_amount: 50_000,
        city: City.find_by(name: 'Samobor')
      )
    end
    let!(:funding_programme_list) { create_list(:funding_programme, 10) }

    it 'should correctly filter funding programmes' do
      xhr :post, :create, filters: {
        name_or_description: 'Code',
        announcer_city_id: 1,
        min_amount: 5_000,
        max_amount: 55_000,
        company_size_id: 5,
        programme_city_id: City.find_by(name: 'Samobor').id
      }

      expect(decoded_body[:funding_programmes].map { |a| a[:id] })
        .to match_array(Array(funding_programme.id))
    end
  end
end
