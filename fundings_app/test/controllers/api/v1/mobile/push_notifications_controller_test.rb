require 'test_helper'

class Api::V1::Mobile::PushNotificationsControllerTest < ActionController::TestCase
  test "should get add_push_id_to_user" do
    get :add_push_id_to_user
    assert_response :success
  end

  test "should get set_flag" do
    get :set_flag
    assert_response :success
  end

end
