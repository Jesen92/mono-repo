require 'test_helper'

class FundingProgrammesControllerTest < ActionController::TestCase
  setup do
    @funding_programme = funding_programmes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:funding_programmes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create funding_programme" do
    assert_difference('FundingProgramme.count') do
      post :create, funding_programme: { beginning_date: @funding_programme.beginning_date, city_id: @funding_programme.city_id, cofinancing_percentage: @funding_programme.cofinancing_percentage, county_id: @funding_programme.county_id, description: @funding_programme.description, end_date: @funding_programme.end_date, funding_programme_link: @funding_programme.funding_programme_link, max_amount: @funding_programme.max_amount, min_amount: @funding_programme.min_amount, name: @funding_programme.name, programme_type_id: @funding_programme.programme_type_id, purpose_id: @funding_programme.purpose_id }
    end

    assert_redirected_to funding_programme_path(assigns(:funding_programme))
  end

  test "should show funding_programme" do
    get :show, id: @funding_programme
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @funding_programme
    assert_response :success
  end

  test "should update funding_programme" do
    patch :update, id: @funding_programme, funding_programme: { beginning_date: @funding_programme.beginning_date, city_id: @funding_programme.city_id, cofinancing_percentage: @funding_programme.cofinancing_percentage, county_id: @funding_programme.county_id, description: @funding_programme.description, end_date: @funding_programme.end_date, funding_programme_link: @funding_programme.funding_programme_link, max_amount: @funding_programme.max_amount, min_amount: @funding_programme.min_amount, name: @funding_programme.name, programme_type_id: @funding_programme.programme_type_id, purpose_id: @funding_programme.purpose_id }
    assert_redirected_to funding_programme_path(assigns(:funding_programme))
  end

  test "should destroy funding_programme" do
    assert_difference('FundingProgramme.count', -1) do
      delete :destroy, id: @funding_programme
    end

    assert_redirected_to funding_programmes_path
  end
end
