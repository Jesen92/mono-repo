require 'test_helper'

class CompanySizesControllerTest < ActionController::TestCase
  setup do
    @company_size = company_sizes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:company_sizes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create company_size" do
    assert_difference('CompanySize.count') do
      post :create, company_size: { name: @company_size.name }
    end

    assert_redirected_to company_size_path(assigns(:company_size))
  end

  test "should show company_size" do
    get :show, id: @company_size
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @company_size
    assert_response :success
  end

  test "should update company_size" do
    patch :update, id: @company_size, company_size: { name: @company_size.name }
    assert_redirected_to company_size_path(assigns(:company_size))
  end

  test "should destroy company_size" do
    assert_difference('CompanySize.count', -1) do
      delete :destroy, id: @company_size
    end

    assert_redirected_to company_sizes_path
  end
end
