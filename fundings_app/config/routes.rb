Rails.application.routes.draw do

  get 'offers/show'

  get 'offers/index'

  namespace :api do
  namespace :v1 do
      post 'favorites/create'
      post 'favorites/show'
      post 'favorites/destroy'
      post 'users/show'
      post 'users/create'
      post 'users/update'
      post 'users/destroy'
      post 'sessions/create'
      post 'sessions/destroy'
      post 'fundings/show'
      post 'fundings/index'
      post 'task/show'
      post 'task/create'
      post 'task/update'
      post 'offers/create'

      post 'notifications/set_flag'
      resource :filter, only: [:show, :create]
      resource :sms_confirmation, only: :create

      namespace :mobile do
        post 'ios_push_notifications/add_push_id_to_user'
        post 'ios_push_notifications/set_flag'
        post 'favorites/create'
        post 'favorites/show'
        post 'favorites/destroy'
        post 'users/show'
        post 'users/create'
        post 'users/update'
        post 'users/destroy'
        post 'android_sessions/create'
        post 'android_sessions/destroy'
        post 'ios_sessions/create'
        post 'ios_sessions/destroy'
        post 'fundings/show'
        post 'fundings/index'
        post 'android_push_notifications/set_flag'
        post 'android_push_notifications/add_push_id_to_user'

        resource :filter, only: [:show, :create]

        resources :users do
          post :check_oib, on: :collection
          post :reset_password, on: :collection
        end
      end

      resources :users do
        post :check_oib, on: :collection
        post :reset_password, on: :collection
      end
    end
  end

  devise_for :users, controllers: {registrations: 'api/v1/users', confirmations: 'api/v1/confirmations'}
  devise_for :admins, controllers: {sessions: 'admins/sessions', registrations: 'admins/registrations', confirmations: 'admins/confirmations'}

  resources :notifications
  resources :offers
  resources :counties
  resources :cities
  resources :programme_types
  resources :purposes
  resources :company_sizes
  resources :companies
  resources :funding_programmes do
    put :delete_document, on: :collection
  end

  #devise_for :companies
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'funding_programmes#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products

  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
