Bugsnag.configure do |config|
  config.api_key = ENV['bugsnag']
  config.notify_release_stages = ['production']
end
