# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170425144816) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "cities", force: :cascade do |t|
    t.integer "county_id"
    t.string  "name"
    t.string  "city_zip_code"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "oib"
    t.string   "address"
    t.date     "license_expires"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "city_id"
    t.integer  "company_size_id"
  end

  create_table "company_sizes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "counties", force: :cascade do |t|
    t.string "name"
  end

  create_table "eligible_users", force: :cascade do |t|
    t.integer  "company_size_id"
    t.integer  "funding_programme_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "funding_programme_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "funding_programmes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "beginning_date"
    t.datetime "end_date"
    t.integer  "cofinancing_percentage"
    t.integer  "purpose_id"
    t.integer  "programme_type_id"
    t.integer  "city_id"
    t.integer  "county_id"
    t.decimal  "min_amount",             precision: 10, scale: 2
    t.decimal  "max_amount",             precision: 10, scale: 2
    t.string   "funding_programme_link"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "status"
    t.boolean  "croatia"
    t.string   "end_term"
    t.integer  "to_cofinancing"
    t.integer  "announcer_city_id"
    t.integer  "announcer_county_id"
    t.boolean  "announcer_croatia"
  end

  create_table "offers", force: :cascade do |t|
    t.string   "model"
    t.string   "reference_number"
    t.integer  "amount"
    t.integer  "user_id"
    t.string   "description"
    t.integer  "count"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "programme_documents", force: :cascade do |t|
    t.integer  "funding_programme_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
  end

  create_table "programme_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purposes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "task_choices", force: :cascade do |t|
    t.text     "description"
    t.integer  "points"
    t.integer  "task_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "task_groups", force: :cascade do |t|
    t.integer  "task_phase_id"
    t.string   "max_points"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "name"
  end

  create_table "task_phases", force: :cascade do |t|
    t.integer  "task_section_id"
    t.integer  "max_points"
    t.integer  "min_points"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "name"
  end

  create_table "task_sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "min_points"
    t.text     "description"
    t.integer  "max_points"
    t.integer  "funding_programme_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.boolean  "radio_button"
  end

  create_table "tasks", force: :cascade do |t|
    t.text     "description"
    t.integer  "task_section_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "task_group_id"
    t.string   "verification_source"
    t.integer  "task_phase_id"
    t.boolean  "yes_no"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                      default: "", null: false
    t.string   "encrypted_password",         default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",              default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "company_id"
    t.string   "auth_token"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "android_auth_token"
    t.string   "ios_auth_token"
    t.string   "ios_push"
    t.string   "android_push"
    t.boolean  "ios_notifications_flag"
    t.boolean  "android_notifications_flag"
    t.string   "phone_number"
    t.boolean  "email_notifications_flag"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
