class AddAnnouncerCityIdAndAnnouncerCountyIdAndAnnouncerCroatiaToFundingProgrammes < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :announcer_city_id, :integer
    add_column :funding_programmes, :announcer_county_id, :integer
    add_column :funding_programmes, :announcer_croatia, :boolean
  end
end
