class AddAndroidAuthTokenAndIosAuthTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :android_auth_token, :string
    add_column :users, :ios_auth_token, :string
  end
end
