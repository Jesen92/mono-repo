class AddIsDevelopmentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_development, :boolean
  end
end
