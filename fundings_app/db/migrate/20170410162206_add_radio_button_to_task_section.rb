class AddRadioButtonToTaskSection < ActiveRecord::Migration
  def change
    add_column :task_sections, :radio_button, :boolean
  end
end
