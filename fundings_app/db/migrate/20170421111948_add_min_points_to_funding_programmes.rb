class AddMinPointsToFundingProgrammes < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :min_points, :integer
  end
end
