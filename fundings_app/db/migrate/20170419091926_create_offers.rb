class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :model
      t.string :reference_number
      t.integer :amount
      t.integer :user_id
      t.string :description
      t.integer :count

      t.timestamps null: false
    end
  end
end
