class AddNameToTaskPhase < ActiveRecord::Migration
  def change
    add_column :task_phases, :name, :string
  end
end
