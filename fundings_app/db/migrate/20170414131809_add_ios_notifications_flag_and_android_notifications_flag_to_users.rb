class AddIosNotificationsFlagAndAndroidNotificationsFlagToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ios_notifications_flag, :boolean
    add_column :users, :android_notifications_flag, :boolean
  end
end
