class CreateEligibleUsers < ActiveRecord::Migration
  def change
    create_table :eligible_users do |t|
      t.integer :company_size_id
      t.integer :funding_programme_id

      t.timestamps null: false
    end
  end
end
