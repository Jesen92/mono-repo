class CreateTaskPhases < ActiveRecord::Migration
  def change
    create_table :task_phases do |t|
      t.integer :task_section_id
      t.integer :max_points
      t.integer :min_points

      t.timestamps null: false
    end
  end
end
