class AddStatusToFundingProgramme < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :status, :string
  end
end
