class AddCityIdAndCompanySizeIdToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :city_id, :integer
    add_column :companies, :company_size_id, :integer
  end
end
