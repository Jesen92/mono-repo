class AddEndTermToFundingProgrammes < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :end_term, :string
  end
end
