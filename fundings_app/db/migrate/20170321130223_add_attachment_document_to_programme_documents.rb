class AddAttachmentDocumentToProgrammeDocuments < ActiveRecord::Migration
  def self.up
    change_table :programme_documents do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :programme_documents, :document
  end
end
