class CreateTaskSections < ActiveRecord::Migration
  def change
    create_table :task_sections do |t|
      t.string :name
      t.integer :min_points
      t.text :description
      t.integer :max_points
      t.integer :funding_programme_id

      t.timestamps null: false
    end
  end
end
