class AddTaskPhaseIdAndYesNoToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :task_phase_id, :integer
    add_column :tasks, :yes_no, :boolean
  end
end
