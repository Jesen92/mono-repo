class AddTaskGroupIdAndVerificationSourceToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :task_group_id, :integer
    add_column :tasks, :verification_source, :string
  end
end
