class CreateFundingProgrammes < ActiveRecord::Migration
  def change
    create_table :funding_programmes do |t|
      t.string :name
      t.text :description
      t.datetime :beginning_date
      t.datetime :end_date
      t.integer :cofinancing_percentage
      t.integer :purpose_id
      t.integer :programme_type_id
      t.integer :city_id
      t.integer :county_id
      t.decimal :min_amount, precision: 10, scale: 2
      t.decimal :max_amount, precision: 10, scale: 2
      t.string :funding_programme_link

      t.timestamps null: false
    end
  end
end
