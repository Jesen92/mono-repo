class CreateTaskGroups < ActiveRecord::Migration
  def change
    create_table :task_groups do |t|
      t.integer :task_phase_id
      t.string :max_points

      t.timestamps null: false
    end
  end
end
