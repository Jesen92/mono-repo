class AddToCofinancingToFundingProgrammes < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :to_cofinancing, :integer
  end
end
