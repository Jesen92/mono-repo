class CreateProgrammeTypes < ActiveRecord::Migration
  def change
    create_table :programme_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
