class CreateProgrammeDocuments < ActiveRecord::Migration
  def change
    create_table :programme_documents do |t|
      t.integer :funding_programme_id

      t.timestamps null: false
    end
  end
end
