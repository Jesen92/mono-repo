class AddAttachmentAvatarToPurposes < ActiveRecord::Migration
  def self.up
    change_table :purposes do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :purposes, :avatar
  end
end
