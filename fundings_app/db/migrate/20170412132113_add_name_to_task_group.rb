class AddNameToTaskGroup < ActiveRecord::Migration
  def change
    add_column :task_groups, :name, :string
  end
end
