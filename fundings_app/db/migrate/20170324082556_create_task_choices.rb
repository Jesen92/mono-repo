class CreateTaskChoices < ActiveRecord::Migration
  def change
    create_table :task_choices do |t|
      t.text :description
      t.integer :points
      t.integer :task_id

      t.timestamps null: false
    end
  end
end
