class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :oib
      t.string :address
      t.date :license_expires

      t.timestamps null: false
    end
  end
end
