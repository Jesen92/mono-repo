class AddIosPushAndAndroidPushToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ios_push, :string
    add_column :users, :android_push, :string
  end
end
