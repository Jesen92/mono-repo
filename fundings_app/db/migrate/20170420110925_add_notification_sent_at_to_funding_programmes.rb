class AddNotificationSentAtToFundingProgrammes < ActiveRecord::Migration
  def change
    add_column :funding_programmes, :notification_sent_at, :datetime
  end
end
