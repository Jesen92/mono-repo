class AddEmailNotificationsFlagToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email_notifications_flag, :boolean
  end
end
