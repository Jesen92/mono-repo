class AddCityZipCodeToCity < ActiveRecord::Migration
  def change
    add_column :cities, :city_zip_code, :string
  end
end
