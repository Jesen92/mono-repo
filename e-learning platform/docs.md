#API Docs Roche
API documentation for Roche e-learning platform

# Group Controllers


## Courses [/courses]


###  [GET /api/v1/courses/index]


+ Request should return success
**GET**&nbsp;&nbsp;`/api/v1/courses/index`

    + Headers



+ Response 200

    + Headers

            Content-Type: application/json; charset=utf-8

    + Body

            {
              "courses": [
                {
                  "id": 5671,
                  "title": "Example course",
                  "description": "Descritpion text",
                  "picture": null
                },
                {
                  "id": 5672,
                  "title": "Example course",
                  "description": "Descritpion text",
                  "picture": null
                },
                {
                  "id": 5673,
                  "title": "Example course",
                  "description": "Descritpion text",
                  "picture": null
                },
                {
                  "id": 5674,
                  "title": "Example course",
                  "description": "Descritpion text",
                  "picture": null
                },
                {
                  "id": 5675,
                  "title": "Example course",
                  "description": "Descritpion text",
                  "picture": null
                }
              ]
            }

### Get single course [POST /api/v1/courses/show]


+ Request should return FIRST course params WITHOUT user specific params
**POST**&nbsp;&nbsp;`/api/v1/courses/show`

    + Headers

            Content-Type: application/x-www-form-urlencoded
