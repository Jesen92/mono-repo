require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get saml_login" do
    get sessions_saml_login_url
    assert_response :success
  end

end
