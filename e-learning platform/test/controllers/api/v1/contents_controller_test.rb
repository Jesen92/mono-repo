require 'test_helper'

class Api::V1::ContentsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get api_v1_contents_show_url
    assert_response :success
  end

end
