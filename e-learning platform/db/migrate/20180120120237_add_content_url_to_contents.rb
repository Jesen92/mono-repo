class AddContentUrlToContents < ActiveRecord::Migration[5.1]
  def change
    add_column :contents, :content_url, :string
  end
end
