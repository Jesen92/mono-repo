class AddPdfToCourseQuizCompletions < ActiveRecord::Migration[5.1]
  def change
    add_column :course_quiz_completions, :pdf, :string
  end
end
