class CreateCourseQuizCompletions < ActiveRecord::Migration[5.1]
  def change
    create_table :course_quiz_completions do |t|
      t.integer :quiz_id
      t.integer :user_id
      t.integer :points

      t.timestamps
    end
  end
end
