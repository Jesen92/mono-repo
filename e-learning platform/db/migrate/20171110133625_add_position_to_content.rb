class AddPositionToContent < ActiveRecord::Migration[5.1]
  def change
    add_column :contents, :position, :integer
  end
end
