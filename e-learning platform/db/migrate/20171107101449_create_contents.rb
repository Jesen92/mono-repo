class CreateContents < ActiveRecord::Migration[5.1]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :length
      t.integer :course_id
      t.integer :content_type_id

      t.timestamps
    end
  end
end
