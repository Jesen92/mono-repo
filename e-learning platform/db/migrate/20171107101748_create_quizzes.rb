class CreateQuizzes < ActiveRecord::Migration[5.1]
  def change
    create_table :quizzes do |t|
      t.integer :points_to_pass

      t.timestamps
    end
  end
end
