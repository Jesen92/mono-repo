class CreateCourseEnrollments < ActiveRecord::Migration[5.1]
  def change
    create_table :course_enrollments do |t|
      t.integer :course_id
      t.integer :user_id
      t.integer :points
      t.boolean :passed?

      t.timestamps
    end
  end
end
