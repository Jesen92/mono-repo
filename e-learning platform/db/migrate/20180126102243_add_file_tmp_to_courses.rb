class AddFileTmpToCourses < ActiveRecord::Migration[5.1]
  def change
    add_column :courses, :file_tmp, :string
  end
end
