class AddAuthTokenIpToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :auth_token_ip, :string
  end
end
