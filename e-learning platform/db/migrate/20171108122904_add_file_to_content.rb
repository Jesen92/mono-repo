class AddFileToContent < ActiveRecord::Migration[5.1]
  def change
    add_column :contents, :file, :string
  end
end
