class AddFileTmpToContents < ActiveRecord::Migration[5.1]
  def change
    add_column :contents, :file_tmp, :string
  end
end
