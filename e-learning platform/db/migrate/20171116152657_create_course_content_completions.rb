class CreateCourseContentCompletions < ActiveRecord::Migration[5.1]
  def change
    create_table :course_content_completions do |t|
      t.integer :user_id
      t.integer :content_id
      t.string :stopped_at
      t.boolean :finished

      t.timestamps
    end
  end
end
