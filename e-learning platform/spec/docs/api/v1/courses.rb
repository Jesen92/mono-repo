module Docs
  module V1
    module Courses
      extend Dox::DSL::Syntax

      # define common resource data for each action
      document :api do
        resource 'Courses' do
          endpoint '/courses'
          group 'Controllers'
        end
      end

      # define data for specific action
      document :index do
        action 'Get courses'
      end

      document :show do
        action 'Get single course'
      end
    end
  end
end