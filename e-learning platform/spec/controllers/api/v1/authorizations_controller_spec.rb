require 'rails_helper'

RSpec.describe Api::V1::AuthorizationsController, type: :controller do
  let(:user) { create(:user)}

  before { sign_in_user(user) }

  describe "POST #authorize_user_by_ip_address" do
    context "when Auth token IP address doesn't exist" do
      it "should return status 200" do
        post :authorize_user_by_ip_address, params: { user: { ip_address: '192.168.1.1'}}

        expect(response.status).to eq(200)
      end
    end

    context "when Auth token IP address already exists" do
      it "and is different from address being POST should return status 403" do
        post :authorize_user_by_ip_address, params: { user: { ip_address: '192.168.1.1'}} #when auth_token_ip_address doesn't exist
        post :authorize_user_by_ip_address, params: { user: { ip_address: '192.168.1.2'}} #when you try to POST different auth_token_ip_address with the same auth_token

        expect(response.status).to eq(403)
      end

      it "and is the same as the address being POST should return status 200" do
        post :authorize_user_by_ip_address, params: { user: { ip_address: '192.168.1.1'}}
        post :authorize_user_by_ip_address, params: { user: { ip_address: '192.168.1.1'}} #when you try to POST the same auth_token_ip_address with the same auth_token

        expect(response.status).to eq(200)
      end
    end
  end
end
