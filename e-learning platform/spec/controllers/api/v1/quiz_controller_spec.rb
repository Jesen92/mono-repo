require 'rails_helper'

RSpec.describe Api::V1::QuizController, type: :controller do
  let(:user) { create(:user) }

  let!(:quiz) { create(:quiz_with_questions) }
  let!(:course) { create(:course_with_contents, quiz_id: quiz.id) }

  before {sign_in_user user}

  describe 'POST #show' do
    context "when I get course Quiz when the user course content completion isn't finished" do
      it 'should fail' do
        post :show, params: {quiz: {id: quiz.id}}

        expect(response.status).to eq(403)

        expect(decoded_body[:quiz])
            .to_not match(JSON.parse(QuizSerializer.new(quiz, scope: user).to_json))
      end
    end

    context "when I get course Quiz when the user course content completion is finished" do
      it 'should be successfull' do
        course.contents.map { |content| user.course_content_completions.create(content_id: content.id, finished:true )} #adding quiz course contents to user and setting finished to true - as if user has finished all of the course content

        post :show, params: {quiz: {id: quiz.id}}

        expect(response.status).to eq(200)

        expect(decoded_body[:quiz])
            .to match(JSON.parse(QuizSerializer.new(quiz, scope: user).to_json))
      end
    end
  end

  describe 'POST #create' do
    #before { course.quiz_id = quiz.id}
    #let!(:course_enrollment) { create(:course_enrollment, course_id: course.id, user_id: user.id) }
    it 'when user has SUCESSFULLY finished the quiz' do
      post :create, params: {quiz: {quiz_id: quiz.id, points: 0, passed?: true, answers: quiz.questions.map {|question| question.answers.where(correct: true).pluck(:id)}.to_a } }

      expect(decoded_body).to include(:certificate_url)
    end

    it 'when user has UNSUCESSFULLY finished the quiz' do
      post :create, params: {quiz: {quiz_id: quiz.id, points: 0, passed?: false, answers: []}}

      expect(response.body).to eq("null")
    end
  end
end
