require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  let(:user) { create(:user) }

  let!(:quiz) { create(:quiz_with_questions) }
  let!(:course) { create(:course, quiz_id: quiz.id) }

  let(:user_with_course) { create(:user_with_course, courses: course) }

  describe 'GET #show' do
    it "where user doesn't have any course content completion" do
      sign_in_user user
      get :show

      expect(response.status).to eq(200)

      expect(decoded_body[:user])
          .to match(JSON.parse(UserSerializer.new(user, scope: user).to_json))
    end

    it "where user has course content completion" do
      sign_in_user user_with_course
      get :show

      expect(response.status).to eq(200)

      expect(decoded_body[:user])
          .to match(JSON.parse(UserSerializer.new(user_with_course, scope: user_with_course).to_json))
    end
  end
end
