require 'rails_helper'

RSpec.describe Api::V1::ContentsController, type: :controller do
  let(:user) {create(:user)}
  let!(:user_with_contents) {create(:user_with_contents)}
  let(:user_content) { user_with_contents.contents.first }

  describe "POST #show" do
    context "where user doesn't have any content completion" do
      before { sign_in_user user }
      it 'should return content json params' do
        post :show, params: {content: {id: user_content.id}}

        expect(response.status).to eq(200)

        expect(decoded_body[:content])
            .to match(JSON.parse(ContentSerializer.new(user_content, scope: user).to_json))
      end
    end

    context "where user has content completion" do
      before { sign_in_user user_with_contents }
      it 'should return content json params with user content completion' do

        post :show, params: {content: {id: user_content.id}}

        expect(response.status).to eq(200)

        expect(decoded_body[:content])
            .to match(JSON.parse(ContentSerializer.new(user_content, scope: user_with_contents).to_json))
      end
    end
  end
end