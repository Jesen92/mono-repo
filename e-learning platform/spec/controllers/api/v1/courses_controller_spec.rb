require 'rails_helper'
require 'support/authentication_helper'

RSpec.describe Api::V1::CoursesController, type: :controller do
  include Docs::V1::Courses::Api
  let!(:course_with_contents) { create_list(:course_with_contents, 5) }
  let(:user) { create(:user) }
  let(:user_with_contents) { create(:user_with_contents) }

  before {sign_in_user user}

  context 'User created' do
    it 'should be successful' do
      expect(user.id).to eq(User.last.id)
    end
  end

  describe 'GET #index' do
    it 'should return success', :dox do
      include Docs::V1::Courses::Index
      get :index

      expect(decoded_body[:courses].map {|a| a[:id]})
          .to match_array(Array(course_with_contents.pluck(:id)))
    end
  end

  describe 'POST #show' do
    include Docs::V1::Courses::Show
    context "user didn't start a course" do
      it 'should return FIRST course params WITHOUT user specific params', :dox do
        post :show, params: {course: {id: course_with_contents.first.id}}

        expect(response.status).to eq(200)

        expect(decoded_body[:course])
            .to match(JSON.parse(CourseSerializer.new(course_with_contents.first, scope: user).to_json))
      end
    end

    context "user did start a course" do
      it 'should return the FIRST course params WITH user specific params', :dox do
        sign_in user_with_contents

        post :show, params: {course: {id: course_with_contents.first.id}}

        expect(response.status).to eq(200)

        expect(decoded_body[:course])
            .to match(JSON.parse(CourseSerializer.new(course_with_contents.first, scope: user_with_contents).to_json))
      end
    end
  end
end
