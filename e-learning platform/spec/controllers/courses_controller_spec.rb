require 'rails_helper'

RSpec.describe CoursesController, type: :controller do
  #include Devise::Test::IntegrationHelpers

  let(:admin) { create(:admin) }

  before { sign_in admin, scope: :admin }

  describe 'create new Course' do
    context 'without its dependencies (contents and quiz)' do
      it 'should succeed' do
        expect{
          post :create, params: {course: attributes_for(:course)}
        }.to change(Course, :count).by(1)
      end
    end

    context 'with its dependencies (contents and quiz)' do
      it 'should succeed' do
        #binding.pry
        expect{
          post :create,
               params: {course: attributes_for(:course_with_contents)
                           .merge(quiz_attributes: attributes_for(:quiz_with_questions)
                                                       .merge(questions_attributes: attributes_for_list(:question, 5)))}
        }.to change(Course, :count).by(1)
      end
    end
  end

  describe 'GET #index' do
    it 'should get all courses' do
      courses = create_list(:course, 5)
      get :index
      expect(assigns(:courses)).to eq(courses)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe 'GET #show' do
    it 'should get course by id' do
      course = create(:course)
      get :show, params: {id: course.id}
      expect(assigns(:course)).to eq(course)
    end

    it "renders the #show view" do
      get :show, params: {id: create(:course).id}
      expect(response).to render_template :show
    end
  end
end
