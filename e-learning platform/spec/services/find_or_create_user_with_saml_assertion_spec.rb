#for this test a SAML Assertion is getting fetched from the environment variables (ENV['SAML'])
#saml assertion should be encrypted using Base64 encryption

describe FindOrCreateUserWithSamlAssertion, 'Service object that handles SAML authorization' do
  let!(:user) { FindOrCreateUserWithSamlAssertion.new(ENV['SAML']).find_or_create_user }

  it 'should create new user' do
    expect(user).to eq(User.last)
  end

  it 'should find an existing user' do
    expect(user.id)
        .to eq(FindOrCreateUserWithSamlAssertion.new(ENV['SAML']).find_or_create_user.id)
  end
end
