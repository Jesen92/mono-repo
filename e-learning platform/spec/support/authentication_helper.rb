module AuthenticationHelper
  def sign_in_user user
    request.headers['Authorization'] = "Token token=#{user.auth_token}, identifier=#{user.email}"
  end

  def sign_out_user
    request.headers['Authorization'] = nil
  end
end