FactoryBot.define do
  factory :course do
    title 'Example course'
    description 'Descritpion text'
    sequence(:quiz_id)
    factory :course_with_contents do
      transient do
        contents_count 5
      end

      after(:create) do |course, evaluator|
        create_list(:content, evaluator.contents_count, course: course)
      end
    end
  end
end