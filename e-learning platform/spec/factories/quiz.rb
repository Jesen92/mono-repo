FactoryBot.define do
  factory :quiz do
    points_to_pass 2

    factory :quiz_with_questions do
      transient do
        questions_count 5
      end

      after(:create) do |quiz, evaluator|
        create_list(:question_with_answers, evaluator.questions_count, quiz: quiz)
      end
    end
  end

  factory :question do
    title 'Example question'

    factory :question_with_answers do
      transient do
        answers_count 5
      end

      after(:create) do |question, evaluator|
        create_list(:answer, evaluator.answers_count, question: question)
      end
    end
  end

  factory :answer do
    title 'Example answer'
    correct true
  end
end