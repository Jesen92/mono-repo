FactoryBot.define do
  factory :content do
    title 'Example content'
    length '02:04'
    content_type
    course
  end
end