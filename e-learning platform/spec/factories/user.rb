FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user-#{n}@gmail.com" }
    first_name 'John'
    last_name 'Doe'

    factory :user_with_contents do
      transient do
        contents_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:user_contents, evaluator.contents_count, user: user)
      end
    end

    factory :user_with_course do
      transient do
        courses nil
      end

      after(:create) do |user, evaluator|
        create(:user_course, course: evaluator.courses, user: user)
      end
    end
  end

  factory :user_contents, class: 'CourseContentCompletion' do
    user
    content
    stopped_at '05:05'
    finished false
  end

  factory :user_course, class: 'CourseEnrollment' do
    user
    course
    passed? false
  end
end