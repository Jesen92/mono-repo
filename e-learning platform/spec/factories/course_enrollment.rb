FactoryBot.define do
  factory :course_enrollment do
    sequence(:course_id)
    sequence(:user_id)
  end
end
