# Be sure to restart your server when you modify this file.

# ActiveSupport::Reloader.to_prepare do
#   ApplicationController.renderer.defaults.merge!(
#     http_host: 'example.org',
#     https: false
#   )
# end

ActiveModelSerializers.config.adapter = :json

  class Application < Rails::Application
    config.to_prepare do
      Devise::SessionsController.layout "sign_in"
    end
  end
