CarrierWave.configure do |config|

  config.fog_provider = 'fog/aws'                        # required
  config.fog_credentials = {
      provider:              'AWS',                        # required
      aws_access_key_id:     ENV['AKI'],                        # required
      aws_secret_access_key: ENV['SAK'],                        # required
      region:                ENV['AWS_REGION']
  }
  config.fog_directory  = ENV['BUC']                          # required
  config.fog_attributes = { cache_control: "public, max-age=#{365.day.to_i}" } # optional, defaults to {}
end