Rails.application.routes.draw do

  get 'sessions/saml_login'

  devise_for :users, controllers: { sessions: 'sessions'}
  resources :users
  devise_for :admins, controllers: {
      sessions: 'admins/sessions', registrations: 'admins/registrations'
  }

  namespace :api do
    namespace :v1 do
      get 'users/create'
      get 'users/show'

      get 'courses/index'
      post 'courses/show'

      post 'quiz/show'
      post 'quiz/create'

      post 'contents/show'
      post 'contents/create'

      post 'authorizations/user_saml_authorization'
      post 'authorizations/authorize_user_by_ip_address', 'authorizations#authorize_user_by_ip_address'
    end
  end

  resources :content_types
  resources :sessions

  resources :quizzes do
    get :user_quiz_completion_graph, on: :collection
  end
  resources :contents do
    post :set_position, on: :collection
  end
  resources :courses

  get '/saml/auth' => 'saml_idp#new'
  post '/saml/auth' => 'saml_idp#create'

  root "courses#index"
end
