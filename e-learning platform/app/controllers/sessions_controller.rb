class SessionsController < ApplicationController
  require 'base64'
  require 'nokogiri'
  require 'open-uri'
  skip_before_action :authenticate_admin!
  skip_before_action :verify_authenticity_token

  before_action :authenticate_user!

  def saml_login
    #method required by Roche developers - it return null
  end

  def index
  end

  def show
  end

  def create
    put params[:user][:ip_address] if params[:user].present?
    b64_encoding = Base64.strict_encode64(@user.email+"+"+@user.auth_token)

    if cookies[:course_id].present?
      course_id = cookies[:course_id]
      cookies.delete :course_id
      return redirect_to ENV['URL_FRONT']+"/#!/courses/#{course_id}/#{b64_encoding}"
    end

    redirect_to ENV['URL_FRONT']+"/#!/profile/#{b64_encoding}"
  end

  private

  def authenticate_user!
    cookies[:course_id] ||= params[:id] #storing course_id when checking for SAML response

    super unless params[:SAMLResponse].present?

    @user = FindOrCreateUserWithSamlAssertion.new(params[:SAMLResponse]).find_or_create_user
  end

end
