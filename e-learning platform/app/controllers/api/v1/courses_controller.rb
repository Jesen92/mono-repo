module Api
  module V1
    class CoursesController < AuthorizationsController
      serialization_scope nil

      def index
        courses = Course.all
        render json: courses, each_serializer: CoursesSerializer, root: 'courses'
      end

      def show
        course = Course.find_by(id: single_course_params[:id])
        render json: course, serializer: CourseSerializer, scope: current_user
      end

      private

      def single_course_params
        params.require(:course).permit( :id)
      end
    end
  end
end


