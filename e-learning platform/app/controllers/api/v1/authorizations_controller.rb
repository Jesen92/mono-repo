module Api
  module V1
    class AuthorizationsController < ApiController
      include Devise::Controllers::Helpers

      before_action :authenticate_user_from_auth_token!

      def authorize_user_by_ip_address
        current_user.update(auth_token_ip: user_ip_params[:ip_address]) if current_user.auth_token_ip.blank?
        if current_user.auth_token_ip.eql? user_ip_params[:ip_address]
          render json: {}, status: 200
        else
          render json: {}, status: 403
        end
      end

      private

      # User is being authenticated through JSON header:
      # Authorization1: Token token=token-string, identifier=user-email@gmail.com
      def authenticate_user_from_auth_token!
        user = authenticate_with_http_token do |token, options|
          (respond_with_json_header_missing_error and return) if options['identifier'].blank?
          user = User.find_by(email: options['identifier'])
          user if Devise.secure_compare(token, user.auth_token)
        end

        (respond_with_auth_error and return) if user.blank? #User doesn't exist or its authorization token is not valid

        # devise, gives you current_user helper method
        sign_in(user, store: false) #current_user
      end

      ######################## Sign in error
      def auth_errors
        @auth_error_message ||= "User doesn't exist or has an invalid authentication token"
        { id: [@auth_error_message], detail: 'not authenticated' }
      end

      def respond_with_auth_error
        render json: { errors: auth_errors }, status: :unauthorized
      end
      ########################

      ######################## JSON Header identifier is missing
      def json_header_errors
        @auth_error_message ||= "Missing JSON header identifier attributes"
        { id: [@auth_error_message], detail: 'JSON header has a missing attribute' }
      end

      def respond_with_json_header_missing_error
        render json: { errors: json_header_errors }, status: :unauthorized
      end
      ########################

      def user_ip_params
        params.require(:user).permit(:ip_address)
      end
    end
  end
end

