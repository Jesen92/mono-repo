module Api
  module V1
    class UsersController < AuthorizationsController

      def show
        render json: current_user, serializer: UserSerializer, scope: current_user
      end

    end
  end
end

