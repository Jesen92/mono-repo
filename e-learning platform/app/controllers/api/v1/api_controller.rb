module Api
  module V1
    class ApiController < ApplicationController #Api Wrapper
      skip_before_action :verify_authenticity_token, :authenticate_admin!
    end
  end
end

