module Api
  module V1
    class QuizController < AuthorizationsController

      def show
        quiz = Quiz.find_by(id: show_params[:id])

        if can_user_partake_quiz?(quiz.course)
          render json: quiz
        else
          render json: {}, status: 403
        end
      end

      def create
        render json: UserQuizCompletion.new(params, current_user).create_update
      end

      private

      def show_params
        params.require(:quiz).permit( :id)
      end

      def can_user_partake_quiz?(course)
        current_user.course_content_completions.where(finished: true)
            .map {|user_content| user_content if user_content.content.course.id == course.id}.count == course.contents.count
      end
    end
  end
end
