module Api
  module V1
    class ContentsController < AuthorizationsController

      def show
        content = Content.find_by(id: contents_params[:id])
        render json: content, serializer: ContentSerializer, scope: current_user
      end

      def create
        render json: create_user_content.to_json
      end

      private

      def contents_params
        params.require('content').permit( :id)
      end

      def create_content_params
        params.require('content').permit(:content_id, :stopped_at, :finished)
      end

      def create_user_course_params
        params.require('content').permit(:course_id)
      end

      def create_user_content
        user_content = current_user.course_content_completions.find_by(content_id: create_content_params[:content_id])
        (user_content.update(create_content_params) and return user_content) if user_content.present?
        
        current_user.course_enrollments
          .create({:course_id => create_user_course_params[:course_id], points: 0}) unless current_user.course_enrollments
            .find_by(course_id: create_user_course_params[:course_id]).present?

        current_user.course_content_completions.create(create_content_params)
      end
    end
  end
end

