class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :set_gon_counter, only: [:edit, :new]

  # GET /courses
  def index
    @courses = Course.all

    @courses_grid = CoursesGrid.new(params[:courses_grid]) do |scope|
      scope.page(params[:page]).per(5)
    end
  end

  # GET /courses/1
  def show
  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  def create
    @course = Course.new(course_params)

    if @course.save
      redirect_to @course, notice: 'Course was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /courses/1
  def update
    @course.remove_file! if course_params[:remove_file] == "1"
    @course.remove_picture! if course_params[:remove_picture] == "1"
    if @course.update(course_params)
      redirect_to @course, notice: 'Course was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /courses/1
  def destroy
    @course.destroy
    redirect_to courses_url, notice: 'Course was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_gon_counter
    gon.mycounter = 1
  end

  def set_course
    @course = Course.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def course_params
    params.require(:course).permit(:id ,:title, :picture, :remove_picture, :description, :file,
                                   :contents_attributes => content_attributes, :quiz_attributes => quiz_attributes)
  end

  def quiz_attributes
    [
        :id, :_destroy, :points_to_pass,
        :questions_attributes => question_attributes
    ]
  end

  def question_attributes
    [
        :id, :_destroy, :title,
        :answers_attributes => answer_attributes
    ]
  end

  def answer_attributes
    [
        :id, :_destroy, :title, :correct
    ]
  end

  def content_attributes
    [
        :id, :_destroy, :title, :length, :description, :content_type_id, :file, :content_url
    ]
  end
end
