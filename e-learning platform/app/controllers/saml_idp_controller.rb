require 'ruby-saml-idp'

class SamlIdpController < SamlIdp::IdpController #SAML IDP used for testing in development

  def idp_authenticate(email, password)
    false
  end

  def idp_make_saml_response(user)
    uri = URI.parse('http://localhost:3000')
    http = Net::HTTP.new(uri.host, uri.port)
    http.post(api_v1_authorizations_user_saml_authorization_url ,encode_SAMLResponse("you@example.com", {:audience_uri => "http://localhost:3000/users/saml/auth"}))
  end
end
