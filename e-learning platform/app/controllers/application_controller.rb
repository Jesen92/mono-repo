class ApplicationController < ActionController::Base
  require 'fog/aws'
  protect_from_forgery with: :exception
  before_action :authenticate_admin!
end
