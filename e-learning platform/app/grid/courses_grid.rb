class CoursesGrid
  include Datagrid

  scope do
    Course.order('created_at DESC').includes(:contents)
  end

  filter(:title, :header => "Title (contains)") { |value| where("title ilike '%#{value}%'") }

  filter(:created_at, :date, :range => true)
  filter(:updated_at, :date, :range => true)

  column(:id)
  column(:title, html: true) do |value|
    link_to value.title, course_path(value)
  end
  column(:created_at) do |value|
    value.created_at.strftime("%d.%m.%Y %H:%M")
  end

  column(:updated_at) do |value|
    value.updated_at.strftime("%d.%m.%Y %H:%M")
  end


  column(:actions, html: true) do |course|
    link_to( "Edit", edit_course_path(course)) +" | "+ link_to('Destroy', course, method: :delete, data: { confirm: 'Are you sure?' } )
  end
end