class FindOrCreateUserWithSamlAssertion
  require 'base64'
  require 'nokogiri'

  def initialize(saml_assertion)
    @saml_assertion = saml_assertion
  end

  def find_or_create_user
    extract_saml_attributes
    regenerate_auth_token_or_create_user
  end

  private

  attr_reader :saml_assertion

  def extract_saml_attributes
    decoded_assertion = Base64.decode64(saml_assertion)
    @saml_attributes = OneLogin::RubySaml::Response.new(decoded_assertion).attributes
  end

  def regenerate_auth_token_or_create_user
    user = User.find_by(email: @saml_attributes[:email])
    user = create_new_user_from_SAML_response_info unless user.present?
    user.regenerate_auth_token!
    user
  end

  def create_new_user_from_SAML_response_info
    user = User.new(email: @saml_attributes[:email],
        first_name: @saml_attributes["first-name"],
        last_name: @saml_attributes["family-name"])
        
    user.save
    user
  end
end