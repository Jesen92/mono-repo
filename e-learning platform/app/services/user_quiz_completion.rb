class UserQuizCompletion
  def initialize(params, user)
    @user = user
    @params = create_user_quiz_params(params)
    @answers = @params[:answers]
  end

  def create_update
    create_user_quiz.to_json
  end

  private

  attr_reader :user, :params, :answers

  def create_user_quiz_params(params)
    params.require(:quiz).permit( :quiz_id, :points, :passed?, :answers => [:id])
  end

  def create_user_quiz
    find_quiz_course

    user_quiz = user.course_quiz_completions.find_by(quiz_id: params[:quiz_id])
    ( update_quiz_course_completion(user_quiz) and return (create_pdf_certificate(user_quiz) if true? params[:passed?])) if user_quiz.present?
    update_user_course
    create_user_answers
    user_quiz = user.course_quiz_completions.create(params.except(:passed?, :answers))
    create_pdf_certificate(user_quiz) if true? params[:passed?]
  end

  def update_quiz_course_completion(user_quiz)
    update_user_course
    update_user_answers
    user_quiz.update({:points => params[:points]})
  end

  def update_user_course
    user_course = user.course_enrollments.find_by(course_id: @course.id)
    (user_course.update(points: params[:points], passed?: params[:passed?]) and return) if user_course.present?
    user.course_enrollments.create(params.except(:quiz_id, :answers).merge({course_id: @course.id}))
  end

  def update_user_answers
    answers_ids = @course.quiz.questions.map {|question| question.answers.pluck(:id)}.flatten
    UserQuestionAnswer.where(user_id: user.id).where(answer_id: answers_ids).delete_all
    create_user_answers
  end

  def create_user_answers
    answers.map { |answer| UserQuestionAnswer.create(user_id: user.id, answer_id: answer[:id])} unless answers.blank?
  end

  def find_quiz_course
    @course = Course.find_by(quiz_id: params[:quiz_id])
  end

  def create_pdf_certificate(user_quiz)
    CreateAndEmailCourseCertificate.new(user_quiz).create_and_email_course_certificate
  end

  def true?(obj)
    obj.to_s == "true"
  end
end