class CreateAndEmailCourseCertificate
  def initialize(user_course_quiz_completion)
    @user_quiz = user_course_quiz_completion
    @user = user_course_quiz_completion.user
  end

  def create_and_email_course_certificate
    create_pdf_certificate
    #email_pdf_certificate #TODO uncomment to e-mail the certificate
  end

  private

  attr_accessor :user_quiz
  attr_reader :user

  def create_pdf_certificate
    #For testing purposes just return the presented hash if user has successfully finished the quiz
    (return {:certificate_url => "https://certificate-url.com"} if Rails.env.test?)
    
    av = ActionView::Base.new
    av.view_paths = ActionController::Base.view_paths

    # need these in case your view constructs any links or references any helper methods.
    av.class_eval do
      include Rails.application.routes.url_helpers
      include ApplicationHelper
    end
    
    pdf_name = "Roche_certificate_"+(0...12).map { ('a'..'z').to_a[rand(26)] }.join
    pdf_html = av.render pdf: pdf_name, layout: 'layouts/pdf', template: "pdf/pdf_certificate",
        locals: {user: user, course: user_quiz.quiz.course.title}, encoding: "UTF-8"

    doc_pdf = WickedPdf.new.pdf_from_string(
        pdf_html,
        page_size: 'A4',
        orientation: 'Landscape',
        background: true,
        no_background: false
    )

    tempfile = Tempfile.new([pdf_name, '.pdf'], Rails.root.join('tmp'))
    tempfile.binmode
    tempfile.write doc_pdf
    tempfile.close

    # Attach that tempfile to the invoice
    unless doc_pdf.blank?
      user_quiz.update(:pdf => File.open(tempfile.path))
      tempfile.unlink
    end

    {:certificate_url => user_quiz.pdf.url}
  end

  def email_pdf_certificate
    PdfCertificateMailer.send_pdf_certificate(user_quiz).deliver_now
  end
end