class CoursesSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :picture

  def picture
    object.picture.url unless object.picture.nil?
  end
end
