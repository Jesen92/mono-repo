class AnswersSerializer < ActiveModel::Serializer
  attributes :id, :title, :correct
end
