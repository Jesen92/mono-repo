class QuestionsSerializer < ActiveModel::Serializer
  attributes :id, :title
  attribute :answers
end
