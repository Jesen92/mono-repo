class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name
  attribute :courses

  def courses
    return nil if object.courses.blank?
    object.courses.map do |course|
      AboutUserCourseSerializer.new(course, scope: scope)
    end
  end
end
