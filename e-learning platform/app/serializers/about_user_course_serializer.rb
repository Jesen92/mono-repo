class AboutUserCourseSerializer < ActiveModel::Serializer
  attributes :id, :title, :picture
  attribute :quiz_points
  attribute :quiz_points_max
  attribute :percentage_completed
  attribute :certificate_url

  def quiz_points_max
    object.quiz.questions.count
  end

  def quiz_points
    (return nil if set_user_course_enrollments.blank?)
    @user_course_enrollment.points unless @user_course_enrollment.blank?
  end

  def percentage_completed
    return 100 if @user_course_enrollment.passed?
    user_contents = scope.course_content_completions.where(finished: true)
    return 5 if user_contents.blank?
    ( (user_contents.map {|user_content| user_content if user_content.content.course.id == object.id}.count.to_f) / (object.contents.count+1).to_f ).round(2)*100
  end

  def picture
    object.picture.url if object.picture.present?
  end

  def certificate_url
    (return nil if set_user_quiz_completion.blank? )
    @user_quiz_completion.pdf.url unless @user_quiz_completion.pdf.blank?
  end

  def set_user_quiz_completion
    @user_quiz_completion = scope.course_quiz_completions.find_by(quiz_id: object.quiz_id)
  end

  def set_user_course_enrollments
    @user_course_enrollment = scope.course_enrollments.find_by(course_id: object.id)
  end
end
