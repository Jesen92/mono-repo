class ContentSerializer < ActiveModel::Serializer
  attributes :id, :content_number ,:description, :title, :length, :course_id, :content_type,:previous_content_id
  attribute :next_content_id, if: :next_content_id
  attributes :finished, :stopped_at
  attribute :next_quiz_id, unless: :next_content_id
  attribute :content_url

   def content_type
    object.content_type.name
  end

  def content_number
    object.position
  end

  def set_user_content
    @user_content = scope.course_content_completions.find_by(content_id: object.id) unless scope.nil?
  end

  def stopped_at
    set_user_content
    @user_content.stopped_at if @user_content.present?
  end

  def finished
    set_user_content
    @user_content.finished if @user_content.present?
  end

  def next_content_id
    return nil if object.lower_item.blank?
    object.lower_item.id
  end

  def previous_content_id
    return nil if object.higher_item.blank?
    object.higher_item.id
  end

  def next_quiz_id
    object.course.quiz.id unless object.course.quiz.blank?
  end

  def content_url
    return object.content_url if object.content_type.name.include? "Present"
    object.file.url unless object.file.blank?
  end
end
