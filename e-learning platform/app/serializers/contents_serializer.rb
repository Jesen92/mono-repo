class ContentsSerializer < ActiveModel::Serializer
  attributes :id, :description, :title, :length, :course_id, :position, :finished, :stopped_at
  attribute :content_type

  def content_type
    object.content_type.name
  end

  def set_user_content
    #binding.pry
    @user_content = scope.course_content_completions.find_by(content_id: object.id) unless scope.nil?
  end

  def stopped_at
    set_user_content
    @user_content.stopped_at if @user_content.present?
  end

  def finished
    set_user_content
    @user_content.finished if @user_content.present?
  end
end
