class CourseSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :picture_url, :quiz_id
  attribute :contents, key: 'curriculum'
  attribute :introduction_video_url

  def introduction_video_url
    object.file.url unless object.file.nil?
  end

  def picture_url
    object.picture.url unless object.picture.nil?
  end

  def contents
    object.contents.order('position ASC').map do |content|
      ContentsSerializer.new(content, scope: scope)
    end
  end
end
