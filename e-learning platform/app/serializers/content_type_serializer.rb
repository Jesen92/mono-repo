class ContentTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
