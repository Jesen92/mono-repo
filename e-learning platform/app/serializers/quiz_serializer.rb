class QuizSerializer < ActiveModel::Serializer
  attributes :id, :points_to_pass, :max_points, :course_title, :course_id
  attribute :questions

  def max_points
    object.questions.count
  end

  def questions
    object.questions.map do |question|
      QuestionsSerializer.new(question)
    end
  end

  def course_title
    object.course.title unless object.course.blank?
  end

  def course_id
    object.course.id unless object.course.blank?
  end
end
