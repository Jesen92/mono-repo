class PdfCertificateMailer < ApplicationMailer
  default from: 'Roche PRO <no-respond@roche.hr>'

  def send_pdf_certificate(user_quiz)
    @user = user_quiz.user
    @course = user_quiz.quiz.course

    attachments["Roche certificate #{@course.title} #{Date.today.strftime("%d.%m.%Y.")}.pdf"] = {
        mime_type: 'application/pdf',
        content: open(user_quiz.pdf.url).read
    }

    mail(to: @user.email, subject: "Course completion certificate #{Date.today}", template_path: 'mailer' ,template_name: 'pdf_certificate')
  end
end
