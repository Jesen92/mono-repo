class Course < ApplicationRecord
  mount_uploader :picture, FileUploader
  mount_uploader :file, FileUploader
  process_in_background :file

  has_many :contents
  accepts_nested_attributes_for :contents, reject_if: :all_blank, allow_destroy: true

  belongs_to :quiz, optional: true
  accepts_nested_attributes_for :quiz, reject_if: :all_blank, allow_destroy: true

  has_many :course_enrollments
  has_many :users, :through => :course_enrollments
end
