class Answer < ApplicationRecord
  belongs_to :question

  has_many :user_question_answers
  has_many :users, :through => :user_question_answers
end
