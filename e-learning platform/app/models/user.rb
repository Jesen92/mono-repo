class User < ApplicationRecord
  attr_accessor :skip_password_validation  # virtual attribute to skip password validation while saving

  has_many :course_enrollments
  has_many :courses, :through => :course_enrollments

  has_many :course_content_completions
  has_many :contents, :through => :course_content_completions

  has_many :course_quiz_completions
  has_many :quizzes, :through => :course_quiz_completions

  has_many :user_question_answers
  has_many :answers, :through => :user_question_answers
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :saml_authenticatable, :trackable

  #devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :trackable

  before_create :generate_auth_token

  def self.create_user_without_password(params)
    self.create(params)
  end

  def regenerate_auth_token!
    generate_auth_token
    self.auth_token_ip = nil
    save
  end

  protected

  def password_required?
    false
  end

  def generate_auth_token
    loop do
      self.auth_token = Devise.friendly_token
      break unless User.exists?(auth_token: auth_token)
    end
  end
end
