class Quiz < ApplicationRecord
  has_one :course

  has_many :questions
  accepts_nested_attributes_for :questions, reject_if: :all_blank, allow_destroy: true

  has_many :course_quiz_completions
  has_many :user, :through => :course_quiz_completions
end
