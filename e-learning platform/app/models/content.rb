class Content < ApplicationRecord
  mount_uploader :file, FileUploader
  process_in_background :file

  has_many :course_content_completions
  has_many :user, :through => :course_content_completions

  belongs_to :content_type
  #belongs_to :quiz
  belongs_to :course, optional: true
  acts_as_list scope: :course
end
