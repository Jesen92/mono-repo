class CourseQuizCompletion < ApplicationRecord
  mount_uploader :pdf, FileUploader

  belongs_to :quiz
  belongs_to :user
end
