module Api
  module V1
    class GroupsController < AuthorizationsController
      before_action :set_group, only: [:show, :edit, :update, :destroy]

      def create
        group = Group.create!(group_params)

        respond_with :api, :v1, json: group, serializer: GroupSerializer, on_error: {
            status: :bad_request, detail: 'Pogreška kod kreiranja grupe! / Grupa već postoji!'
        }
      end

      def index
        respond_with :api, :v1, json: Group.all, each_serializer: GroupSerializer
      end

      def show
        respond_with :api, :v1, json: @group, serializer: GroupSerializer
      end

      def edit
        respond_with :api, :v1, json: @group, serializer: GroupSerializer
      end

      def update
        @group.update(group_params)
        respond_with :api, :v1, json: @group, serializer: GroupSerializer
      end

      def destroy
        @group.destroy
        render json: {notice: {detail: 'Grupa je uspješno izbrisana.'}}
      end

      private

      def set_group
        @group = Group.find(group_params[:id])
      end  

      def group_params
        params.require(:group).permit(:id, :name)
      end

    end
  end
end
