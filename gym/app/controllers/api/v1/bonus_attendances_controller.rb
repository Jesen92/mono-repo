module Api
  module V1
    class BonusAttendancesController < AuthorizationsController
      before_action :set_user, only: [:create]

      def create
        render json: NewBonusAttendanceService.new(@user).perform
      end

      private

      def set_user
        @user = User.find(bonus_attendances_params[:user_id])
      end

      def bonus_attendances_params
        params.require(:bonus_attendance).permit(:user_id)
      end

    end
  end
end
