module Api
  module V1
    class DashboardsController < AuthorizationsController
      respond_to :json

      def index
        respond_with :api, :v1, json: user_status_count, on_error: {
          status: :bad_request, detail: 'Pogreška kod dohvaćanja statistike aktivnosti korisnika!'
      }
      end

      private

      def user_status_count #get user count by predefined statuses
        { status: { active_count: User.where(status: 'active').count,
         inactive_count: User.where(status: 'inactive').count,
          pause_count: User.where(status: 'pause').count}}.to_json
      end
    end
  end
end