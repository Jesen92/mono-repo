module Api
  module V1
    class MembershipTypesController < AuthorizationsController
      before_action :set_membership_type, only: [:show, :edit, :update, :destroy]

      def index
        respond_with :api, :v1, json: MembershipType.all, each_serializer: MembershipTypeSerializer
      end

      def show
        respond_with :api, :v1, json: @membership_type, serializer: MembershipTypeSerializer
      end

      def edit
        respond_with :api, :v1, json: @membership_type, serializer: MembershipTypeSerializer
      end

      def create
        membership_type = MembershipType.create!(membership_type_params)

        respond_with :api, :v1, json: membership_type, serializer: MembershipTypeSerializer, on_error: {
            status: :bad_request, detail: 'Pogreška kod kreiranja članarine! / Članarina već postoji!'
        }
      end

      def update
        @membership_type.update(membership_type_params)

        respond_with :api, :v1, json: @membership_type, serializer: MembershipTypeSerializer
      end


      def destroy
        MembershipType.find(membership_type_params[:id]).destroy
        render json: {notice: {detail: 'Članarina uspješno izbrisana!'}}
      end

      private

      def set_membership_type
        @membership_type = MembershipType.find(membership_type_params[:id])
      end  

      # Never trust parameters from the scary internet, only allow the white list through.
      def membership_type_params
        params.require(:membership_type).permit(
            :id, :name, :max_week_attendance_restriction, :after_hour_restriction
        )
      end
    end
  end
end
