class User < ApplicationRecord
  has_one :membership_type
  has_many :member_attendances, :dependent => :delete_all
  has_many :group_has_users
  has_many :groups, :through => :group_has_users

  has_many :user_has_membership_types
  has_many :membership_types, :through => :user_has_membership_types

  before_update { |user|
    user.status = user.membership_ends_at > DateTime.now ? 'active' : 'inactive' unless user.status.include? 'pause' || user.membership_ends_at.blank?
  }

end
