UPDATE users
SET membership_ends_at = users.updated_at
WHERE users.updated_at IS NOT NULL;

UPDATE users
SET membership_starts_at = users.created_at
WHERE users.created_at IS NOT NULL;