desc 'Setting user statuses according to membership_ends_at field'

task :set_user_statuses => :environment do
  puts "Started setting user statuses"
  User.all.map {|user| user.update(status: user.membership_ends_at.present? && user.membership_ends_at >= DateTime.now() ? 'active' : 'inactive') }
  puts "Done"
end