require 'rails_helper'

RSpec.describe Api::V1::MemberAttendancesController, type: :controller do
  let(:admin) {create(:admin, username: 'adminuser')}
  let!(:user) { create(:user) }
  let!(:membership_type) { create(:membership_type) }
  let!(:member_attendances) { create_list(:member_attendance, 20, user_id: user.id, membership_type_id: membership_type.id) }

  before {sign_in admin}

  describe "GET #index" do
    it "returns all member attendances" do
      get :index

      expect(response.status).to eq(200)
      
      expect(decoded_body[:member_attendances].map {|m| m[:id]}).to match_array(Array(member_attendances.pluck(:id)))
    end

    it "returns last 5 member attendances" do
      get :index, params: {count: '5'}

      expect(response.status).to eq(200)

      expect(decoded_body[:member_attendances].map {|m| m[:id]}).to match_array(Array(member_attendances.last(5).pluck(:id)))
    end
  end      

end  