require 'rails_helper'

RSpec.describe Api::V1::DashboardsController, type: :controller do
  let(:admin) {create(:admin, username: 'adminuser')}
  
  before {sign_in admin}

  describe "GET #index" do
    it "returns status counts" do
      inactive_users = create_list(:user, 6, status: 'inactive')
      pause_users = create_list(:user, 3, status: 'pause',)
      active_users = create_list(:user, 5)

      get :index

      expect(response.status).to eq(200)

      expect(decoded_body[:status])
        .to eq("active_count" => active_users.count, "inactive_count" => inactive_users.count, "pause_count" => pause_users.count)
    end
  end
end
