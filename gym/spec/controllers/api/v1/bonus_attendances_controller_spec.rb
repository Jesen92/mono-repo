require 'rails_helper'

RSpec.describe Api::V1::BonusAttendancesController, type: :controller do
  let(:admin) { create(:admin, username: 'adminuser') }
  let(:user) { create(:user) }

  before {sign_in admin}

  describe 'POST #create' do
    it 'should return that the bonus attendance was successfully added' do 
      post :create, params: { bonus_attendance: { user_id: user.id}}

      expect(response.status).to eq(200)

      expect(decoded_body[:bonus_attendance_approved])
          .to eq(true)
    end

    it 'should return that the bonus attendance was NOT successfully added' do
      post :create, params: { bonus_attendance: { user_id: user.id}}
      post :create, params: { bonus_attendance: { user_id: user.id}} #test is expecting the second response to have bonus_attendance_approved be false

      expect(response.status).to eq(200)

      expect(decoded_body[:bonus_attendance_approved])
          .to eq(false)
    end  
  end  
end
