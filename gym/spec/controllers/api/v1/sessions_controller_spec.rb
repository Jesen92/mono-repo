require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  let(:admin) { create(:admin, username: 'adminuser', password: 'asdf1234') }

  describe 'POST #create' do
    it 'should create new auth token' do
      auth_token = admin.auth_token
      post :create, params: { admin: { username: 'adminuser', password: 'asdf1234' } }

      expect(response.status).to eq(200)

      expect(decoded_body[:admin][:auth_token]).to_not eq(auth_token)
    end  

    it 'should NOT create new auth token' do
      auth_token = admin.auth_token
      post :create, params: { admin: { username: 'adminuser', password: 'wrong_password' } }

      expect(response.status).to eq(400)

      expect(auth_token).to eq(Admin.find(admin.id).auth_token)
    end 
  end  

  describe 'POST #destroy' do 
    it 'should create new auth token' do
      sign_in admin
      auth_token = admin.auth_token
      
      get :destroy
      
      expect(response.status).to eq(200)

      expect(auth_token).to_not eq(Admin.find(admin.id).auth_token)
    end  
  end
end
