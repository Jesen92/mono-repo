FactoryBot.define do
  factory :membership_type do
    name "Gym"
    max_week_attendance_restriction 5
    after_hour_restriction "12:00"
  end
end
