FactoryBot.define do
  factory :member_attendance do
    sequence(:user_id)
    sequence(:membership_type_id)
  end
end  