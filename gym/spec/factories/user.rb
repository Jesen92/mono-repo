FactoryBot.define do
  factory :user do
    email {"user_#{Random.rand(1000).to_s}@factory.com" }
    first_name 'FirstName'
    last_name 'LastName'
    status 'active'
    membership_starts_at DateTime.now-5.days
    membership_ends_at DateTime.now+5.days

  end
end
