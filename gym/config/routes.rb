Rails.application.routes.draw do
  #resources are not used since it is not standard CRUD
  #GET is used only for /index
  #POST is used instead of GET with params, PATCH and PUT

  namespace :api do
    namespace :v1 do
      #API - dashboards controller
      get 'dashboards/index'

      #API - membership types controller
      get 'membership_types/index'
      post 'membership_types/show'
      post 'membership_types/edit'
      post 'membership_types/destroy'
      post 'membership_types/update'
      post 'membership_types/create'
      
      #API - mails controller 
      post 'mails/create'

      #API - user groups controller
      get 'user_groups/index'
      post 'user_groups/show'
      post 'user_groups/create'
      post 'user_groups/destroy'

      #API - bonus atenndances controller
      post 'bonus_attendances/create'

      #API - member attendances controller
      get 'member_attendances/index'
      post 'member_attendances/create'
      post 'member_attendances/destroy'

      #API - notes controller
      get 'notes/index'
      post 'notes/create'
      post 'notes/destroy'

      #API - groups controller
      get 'groups/index'
      post 'groups/show'
      post 'groups/create'
      post 'groups/edit'
      post 'groups/update'
      post 'groups/destroy'
      
      #API - users controller
      get 'users/index'
      get 'users/new'
      post 'users/create'
      post 'users/show'
      post 'users/edit'
      post 'users/update'
      post 'users/destroy'
      
      #API - sessions controller
      get 'sessions/destroy'
      post 'sessions/create'

      resources :admins
    end
  end

  devise_for :admins

end
